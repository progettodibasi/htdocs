function addToList(playerTable) {
  var tr = sessionStorage.getItem('playerTable');
  //alert(playerTable);
  if(tr == null)
    tr = playerTable;
  else
    tr += playerTable;
  tr += "\n";
  sessionStorage.setItem('playerTable', tr);
}

var vettore;

function assignNum(){
  vettore = new Array(sessionStorage.getItem('numPg'));
}

function print() {
  var init = sessionStorage.getItem('playerTable');
  init = init.split("\n");
  init.forEach(function(entry) {
    if(entry != "")
      alert(entry.split(","));
  });
}

function setArray(){
  if(sessionStorage.getItem('numPg') == 0)
    window.location.href='/../create_pg.html';
  assignNum();
  var init = sessionStorage.getItem('playerTable');
  init = init.split("\n");
  for(var i=0; i<sessionStorage.getItem('numPg'); i++){
    if(init[i] != ""){
      vettore[i] = init[i].split(",");
      appendButt(vettore[i][4], vettore[i][3], i);
    }
  }
  showSelectedPg(0);
}

var count2 = 0;

function appendButt(pgName, exp, num){
  var btn = document.createElement("BUTTON");
  var t = document.createTextNode(pgName+" { lvl "+Math.floor(exp/50)+" }");
  btn.value = num;
  btn.style.width="100%";
  btn.onclick = showSelectedPg;
  btn.appendChild(t);
  btn.id = "bottonee" + count2;
  btn.disabled = false;
  document.getElementById('scroll').append(btn);
  count2++;
}

function showSelectedPg(ev){
  if(ev == 0)
    num = ev;
  else {
    num = ev.target.value;
  }
  document.getElementById("id1").value = vettore[num][0];
  document.getElementById("id2").value = vettore[num][0];

  document.getElementById("descr").innerHTML = vettore[num][5];

  document.getElementById('F').innerHTML = vettore[num][6];
  document.getElementById('I').innerHTML = vettore[num][7];
  document.getElementById('C').innerHTML = vettore[num][8];
  document.getElementById('A').innerHTML = vettore[num][9];

  document.getElementById("ATK").innerHTML = vettore[num][10];
  document.getElementById("DEF").innerHTML = vettore[num][11];
  document.getElementById("PER").innerHTML = vettore[num][12];
  document.getElementById("PF").innerHTML = vettore[num][13];

  for(var n = 0; n<count2; n++){
    var stringa2  = "bottonee" + n;
    document.getElementById(stringa2).disabled = false;
  }

  if(ev == 0){
    var first = "bottonee0";
    document.getElementById(first).disabled = true;
  }
  else {
    ev.target.disabled=true;
  }

}
/*SOLUZIONE ALTERNATIVA
function addToList(numTable, playerTable) {
  sessionStorage.setItem(numTable, playerTable);
}

function print() {
  var num = 0;
  while(sessionStorage.getItem(num)){
    var pg = sessionStorage.getItem(num);
    alert(pg);
    num++;
  }
}*/
