function funct1(){
  var c1 = document.getElementById("a").value;
  //var c2 = document.getElementById("b").value;
  var c3 = document.getElementById("c").value;
  if(c1 != "" && c3 != "" && validateEmail(c1)){
    document.getElementById("but1").disabled = false;
  }
  else {
    document.getElementById("but1").disabled = false;
  }
}

function funct2(){
  var c4 = document.getElementById("d").value;
  var c5 = document.getElementById("e").value;
  if(c4 != "" && c5 != ""  && validateEmail(c4)){
    document.getElementById("but2").disabled = false;
  }
  else {
    document.getElementById("but2").disabled = false;
  }
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
