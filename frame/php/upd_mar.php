<!DOCTYPE html>
<?php
  session_start();
  ?>
<html>
  <head>
    <meta charset="utf-8" />
    <meta content="Pagina di gioco" />
    <title>Mercato</title>
    <link rel="icon" href="/css/image/icona.ico" />
    <link href="/frame/css/inv.css" rel="stylesheet" type="text/css" />
  </head>
  <script type="text/javascript" src="../javascript/mark.js"></script>
  <body>
    <pre>
      <?php
      ini_set('display_errors', true);
      error_reporting(E_ALL);
        $param = array($_SESSION["personaggio"]);
        $database = pg_connect("host=localhost port=5432 dbname=dungeonasdb user=postgres password=postgres")
           or die("Impossibile connettersi.");
        $path_query = pg_query($database, "SET search_path TO dungeonasdb");
        ?>
    </pre>
      <?php
        $mar_query = pg_query_params($database, "SELECT I.id, I.oggetto, I.in_vendita, I.scambia, O.descrizione, O.attacco, O.difesa, O.percezione, O.punti_ferita, O.tipo_oggetto, O.punti_attacco
              FROM inventario AS I
              JOIN oggetto AS O ON I.oggetto = O.nome
              WHERE (O.tipo_oggetto = 0
                    OR O.tipo_oggetto = 1
                    OR O.tipo_oggetto = 2
                    OR O.tipo_oggetto = 3)
                    AND
                       (I.in_vendita > 0 AND I.scambia IS NULL)
                    AND
                      I.personaggio <> $1
                    AND
                      NOT((O.tipo_oggetto = 2 OR O.tipo_oggetto = 3) AND equipaggiato)
            ORDER BY O.nome;", $param);
        $mar_length = pg_num_rows($mar_query);

        echo "<script type ='text/javascript'>
              var marList = '';
              sessionStorage.setItem('market', marList);
              sessionStorage.setItem('marDim', $mar_length);
          </script>";
        while ($mar_row = pg_fetch_row($mar_query)) {
          $js_array = json_encode($mar_row);
          echo "<script type='text/javascript'>
            addToMar($js_array);
          </script>";
        }

      $monete_query = pg_query_params($database,"SELECT I.quantita FROM inventario  AS I JOIN oggetto AS O ON I.oggetto = O.nome WHERE I.personaggio = $1 AND O.tipo_oggetto = 4;", $param);
      $monete = pg_fetch_row($monete_query);
      echo "<script type ='text/javascript'>
            sessionStorage.setItem('moneteMark', $monete[0]);
        </script>";

        $sell_query = pg_query_params($database, "SELECT I.id, I.oggetto
          FROM inventario AS I
          JOIN oggetto AS O ON I.oggetto = O.nome
          WHERE (O.tipo_oggetto = 0
          OR O.tipo_oggetto = 1
          OR O.tipo_oggetto = 2
          OR O.tipo_oggetto = 3)
          AND
            NOT((O.tipo_oggetto = 2 OR O.tipo_oggetto = 3) AND equipaggiato)
          AND
            I.personaggio = $1 AND I.in_vendita = 0 AND I.scambia IS NULL
          ORDER BY I.oggetto;", $param);
        $sell_length = pg_num_rows($sell_query);


        echo "<script type ='text/javascript'>
              var sellList = '';
              sessionStorage.setItem('sell', sellList);
              sessionStorage.setItem('sellDim', $sell_length);
          </script>";

        while ($sell_row = pg_fetch_row($sell_query)) {
          $js_array_sell = json_encode($sell_row);
          echo "<script type='text/javascript'>
            addToSell($js_array_sell);
          </script>";
        }

        $back_query =  pg_query_params($database, "SELECT I.id, I.oggetto, I.in_vendita
          FROM inventario AS I
          JOIN oggetto AS O ON I.oggetto = O.nome
          WHERE (O.tipo_oggetto = 0
          OR O.tipo_oggetto = 1
          OR O.tipo_oggetto = 2
          OR O.tipo_oggetto = 3)
          AND
            I.personaggio = $1 AND I.in_vendita <> 0 AND I.scambia IS NULL
          AND
            NOT((O.tipo_oggetto = 2 OR O.tipo_oggetto = 3) AND equipaggiato)
          ORDER BY I.oggetto;", $param);
        $back_length = pg_num_rows($back_query);

        echo "<script type ='text/javascript'>
              var backList = '';
              sessionStorage.setItem('back', backList);
              sessionStorage.setItem('backDim', $back_length);
          </script>";

        while ($back_row = pg_fetch_row($back_query)) {
          $js_array_back = json_encode($back_row);
          echo "<script type='text/javascript'>
            addToBack($js_array_back);
          </script>";
        }


       echo "<script type ='text/javascript'>
         window.location.href='../vendita.html';
        </script>";
        ?>
  </body>
</html>
