<!DOCTYPE html>
<?php
  session_start();
  ?>
<html>
  <head>
    <meta charset="utf-8" />
    <meta content="Pagina di gioco" />
    <title>Naviga</title>
    <link rel="icon" href="/css/image/icona.ico" />
    <link href="/frame/css/inv.css" rel="stylesheet" type="text/css" />
  </head>

  <body>
    <pre>
      <?php
        $database = pg_connect("host=localhost port=5432 dbname=dungeonasdb user=postgres password=postgres")
           or die("Impossibile connettersi.");
        $path_query = pg_query($database, "SET search_path TO dungeonasdb");
        ?>
    </pre>
      <?php
      if ($_POST["itemQnt"] != "-1"){
        $param = array($_SESSION["personaggio"], $_POST["itemNome"], $_POST["itemQnt"]);
        $add_query = pg_query_params($database, "INSERT INTO inventario (personaggio, oggetto, quantita) VALUES ($1, $2, $3);", $param);
      }
      else {
        $param = array($_SESSION["personaggio"], $_POST["itemNome"]);
        $add_query = pg_query_params($database, "INSERT INTO inventario (personaggio, oggetto) VALUES ($1, $2);", $param);

      }
      if(!$add_query){
        echo "<script type ='text/javascript'>
          alert('inventario pieno!');
          window.location.href='../php/show_item.php';
        </script>";
      }
      else{
        $param = array($_POST["itemId"]);
        $add_query = pg_query_params($database, "DELETE FROM posizionato WHERE id = $1;", $param);
        echo "<script type ='text/javascript'>
          window.location.href='../php/show_item.php';
        </script>";
      }
      ?>
  </body>
</html>
