<!DOCTYPE html>
<?php
  session_start();
  ?>
<html>
<head>
  <meta charset="utf-8" />
  <title>Map</title>
  <link rel="icon" href="/css/image/icona.ico" />
  <link href="/frame/css/style.css" rel="stylesheet" type="text/css" />
</head>
<script type="text/javascript" src="../javascript/fsx.js"></script>
<body>
  <pre>
    <?php
      ini_set('display_errors', true);
      error_reporting(E_ALL);
      $param = array($_SESSION["personaggio"]);
      $dungeon = array('','','','','','','','','','','','','','','','','','','','','');
      $database = pg_connect("host=localhost port=5432 dbname=dungeonasdb user=postgres password=postgres")
         or die("Impossibile connettersi.");
      $path_query = pg_query($database, "SET search_path TO dungeonasdb");
      $pg_query = pg_query_params($database, "SELECT stanza, tipo_stanza, status FROM personaggio P JOIN stanza S ON P.stanza = S.codice_numerico WHERE P.id = $1;", $param);
      $root = pg_fetch_row($pg_query);
      $param = array($root[0]);
      $js_array = json_encode($param);
      echo "<script type ='text/javascript'>
        sessionStorage.setItem('pgPos', $js_array);
       </script>";
     ?>
  </pre>
  <?php
    while (true) {
      $root_query = pg_query_params($database, "SELECT da_stanza, tipo_stanza, status FROM passaggio P JOIN stanza S ON P.da_stanza = S.codice_numerico WHERE a_stanza = $1;", $param);
      if ($root_row = pg_fetch_row($root_query)){
        $root = $root_row;
        $param = array($root[0]);
      }
      else break;
    }
    $dungeon[0] = $root;
    for ($i=0; $i < 13; $i++) {
      if ($dungeon[$i]) {
        $param = array($dungeon[$i][0]);
        $map_query = pg_query_params($database, "SELECT a_stanza, tipo_stanza, status FROM passaggio P JOIN stanza S ON P.a_stanza = S.codice_numerico WHERE da_stanza = $1 ORDER BY codice_numerico ASC;", $param);
        if($i == 0){
          $count = $i+1;
          $limit = 4;
        }
        else if(($i >= 1) && ($i <= 4)){
          $count = 5+(($i-1)*2);
          $limit = 2;
        }
        else{
          $count = $i+8;
          $limit = 1;
        }
        for ($j = 0; $j < $limit; $j++)
          if($room = pg_fetch_row($map_query))
            $dungeon[$count+$j] = $room;
      }
    }
    echo "<script type ='text/javascript'>
      var roomList = '';
      sessionStorage.setItem('map', roomList);
     </script>";
    for($i=0; $i < 21; $i++){
      $js_array = json_encode($dungeon[$i]);
      echo "<script type ='text/javascript'>
        mapInit($js_array);
       </script>";
    }
    echo "<script type ='text/javascript'>
      window.location.href='../map.html';
     </script>";
   ?>
</body>
</html>
