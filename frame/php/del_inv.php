<!DOCTYPE html>
<?php
  session_start();
  ?>
<html>
  <head>
    <meta charset="utf-8" />
    <meta content="Pagina di gioco" />
    <title>Inventario</title>
    <link rel="icon" href="/css/image/icona.ico" />
    <link href="/frame/css/inv.css" rel="stylesheet" type="text/css" />
  </head>

  <body>
    <pre>
      <?php
        $param = array($_POST["invId2"]);
        $database = pg_connect("host=localhost port=5432 dbname=dungeonasdb user=postgres password=postgres")
           or die("Impossibile connettersi.");
        $path_query = pg_query($database, "SET search_path TO dungeonasdb");
        ?>
    </pre>
    <?php
      $inv_query = pg_query_params($database, "SELECT delete_item($1);", $param);
      echo "<script type ='text/javascript'>
        window.location.href='upd_inv.php';
      </script>";
    ?>
  </body>
</html>
