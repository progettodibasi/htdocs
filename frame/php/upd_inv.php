<!DOCTYPE html>
<?php
  session_start();
  ?>
<html>
  <head>
    <meta charset="utf-8" />
    <meta content="Pagina di gioco" />
    <title>Inventario</title>
    <link rel="icon" href="/css/image/icona.ico" />
    <link href="/frame/css/inv.css" rel="stylesheet" type="text/css" />
  </head>
  <script type="text/javascript" src="../javascript/inv2.js"></script>
  <body>
    <pre>
      <?php
        $param = array($_SESSION["personaggio"]);
        $database = pg_connect("host=localhost port=5432 dbname=dungeonasdb user=postgres password=postgres")
           or die("Impossibile connettersi.");
        $path_query = pg_query($database, "SET search_path TO dungeonasdb");
        ?>
    </pre>
      <?php
        $inv_query = pg_query_params($database, "SELECT I.oggetto, I.quantita, O.descrizione, O.attacco, O.difesa, O.percezione, O.punti_ferita, O.tipo_oggetto, O.punti_attacco, I.equipaggiato, I.id FROM inventario I JOIN oggetto O ON I.oggetto = O.nome WHERE personaggio = $1 AND tipo_oggetto <> 5 ORDER BY O.tipo_oggetto", $param);
        $inv_length = pg_num_rows($inv_query);
        echo "<script type ='text/javascript'>
              var invList = '';
              sessionStorage.setItem('inventory', invList);
              sessionStorage.setItem('invDim', $inv_length);
          </script>";
        while ($inv_row = pg_fetch_row($inv_query)) {
          $js_array = json_encode($inv_row);
          echo "<script type='text/javascript'>
            addToInv($js_array);
          </script>";
        }
       echo "<script type ='text/javascript'>
         window.location.href='../inventory.html';
        </script>";
        ?>
  </body>
</html>
