<!DOCTYPE html>
<?php
  session_start();
  ?>
<html>
  <head>
    <meta charset="utf-8" />
    <meta content="Pagina di gioco" />
    <title>Naviga</title>
    <link rel="icon" href="/css/image/icona.ico" />
    <link href="/frame/css/inv.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../javascript/nav.js?5"></script>
    </head>

  <body>
    <pre>
      <?php
        $param = array($_SESSION["personaggio"]);
        $database = pg_connect("host=localhost port=5432 dbname=dungeonasdb user=postgres password=postgres")
           or die("Impossibile connettersi.");
        $path_query = pg_query($database, "SET search_path TO dungeonasdb");

        ?>
    </pre>
      <?php
       $obj_query = pg_query_params($database, "SELECT P.id, P.oggetto, P.quantita FROM posizionato P
                                                JOIN personaggio PE ON PE.id = $1 WHERE PE.stanza = P.stanza AND visibilita ORDER BY P.oggetto;", $param);
      $num_row = pg_num_rows($obj_query);
      echo "<script type ='text/javascript'>
            var objList = '';
            sessionStorage.setItem('roomItem', objList);
            sessionStorage.setItem('numItem', $num_row);
        </script>";
      while ($item_row = pg_fetch_row($obj_query)) {
        $js_array = json_encode($item_row);
        echo "<script type ='text/javascript'>
          addToList($js_array);
        </script>";
      }

      $type_query = pg_query_params($database, "SELECT S.tipo_stanza FROM stanza S
                                               JOIN personaggio P ON P.stanza = S.codice_numerico WHERE P.id = $1;", $param);
     $type_fetch = pg_fetch_row($type_query);
     echo "<script type ='text/javascript'>
           sessionStorage.setItem('type', $type_fetch[0]);
       </script>";

       $enemy_query = pg_query_params($database, "SELECT * FROM nemico N
                                                  JOIN collocato C ON N.nome = C.nemico
                                                  JOIN personaggio P ON P.id = $1
                                                  WHERE C.stanza = P.stanza;", $param);
       $en_length = pg_num_rows($enemy_query);
       echo "<script type ='text/javascript'>
            sessionStorage.setItem('enNum', $en_length);
        </script>";

       echo "<script type ='text/javascript'>
         window.location.href='../browse.html';
        </script>";
        ?>
  </body>
</html>
