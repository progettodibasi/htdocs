<!DOCTYPE html>
<?php
  session_start();
  ?>
<html>
  <head>
    <meta charset="utf-8" />
    <meta content="Pagina di gioco" />
    <title>Battaglia</title>
    <link rel="icon" href="/css/image/icona.ico" />
    <link href="/frame/css/inv.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <pre>
      <?php
        ini_set('display_errors', true);
        error_reporting(E_ALL);
        $param = array($_SESSION["personaggio"], $_POST["enemy"]);
        $database = pg_connect("host=localhost port=5432 dbname=dungeonasdb user=postgres password=postgres")
           or die("Impossibile connettersi.");
        $path_query = pg_query($database, "SET search_path TO dungeonasdb");
        ?>
    </pre>
    <?php
    $fight_query = pg_query_params($database, "SELECT battle($1, $2);", $param);
    $logger = pg_fetch_row($fight_query);
    $js_array = json_encode($logger);
    echo "<script type ='text/javascript'>
     sessionStorage.setItem('logger', $js_array);
     window.top.location.reload();
    </script>";
    ?>
  </body>
</html>
