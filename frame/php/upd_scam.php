<!DOCTYPE html>
<?php
  session_start();
  ?>
<html>
  <head>
    <meta charset="utf-8" />
    <meta content="Pagina di gioco" />
    <title>Mercato</title>
    <link rel="icon" href="/css/image/icona.ico" />
    <link href="/frame/css/inv.css" rel="stylesheet" type="text/css" />
  </head>
  <script type="text/javascript" src="../javascript/mark.js"></script>
  <body>
    <pre>
      <?php
      ini_set('display_errors', true);
      error_reporting(E_ALL);
        $param = array($_SESSION["personaggio"]);
        $database = pg_connect("host=localhost port=5432 dbname=dungeonasdb user=postgres password=postgres")
           or die("Impossibile connettersi.");
        $path_query = pg_query($database, "SET search_path TO dungeonasdb");
       ?>
    </pre>
      <?php
        $sca_query = pg_query_params($database, "SELECT I.id, I.oggetto, I.in_vendita, I.scambia, O.descrizione, O.attacco, O.difesa, O.percezione, O.punti_ferita, O.tipo_oggetto, O.punti_attacco
              FROM inventario AS I
              JOIN oggetto AS O ON I.oggetto = O.nome
              WHERE (O.tipo_oggetto = 0
                    OR O.tipo_oggetto = 1
                    OR O.tipo_oggetto = 2
                    OR O.tipo_oggetto = 3)
                    AND
                       (I.in_vendita = 0 AND I.scambia IS NOT NULL)
                    AND
                      I.personaggio <> $1
                    AND
                      NOT((O.tipo_oggetto = 2 OR O.tipo_oggetto = 3) AND equipaggiato)
            ORDER BY O.nome;", $param);
        $sca_length = pg_num_rows($sca_query);

        echo "<script type ='text/javascript'>
              var scaList = '';
              sessionStorage.setItem('scambio', scaList);
              sessionStorage.setItem('scaDim', $sca_length);
          </script>";

        while ($sca_row = pg_fetch_row($sca_query)) {
          $js_array = json_encode($sca_row);
          echo "<script type='text/javascript'>
            addToSca($js_array);
          </script>";
        }

        $sca_control = pg_query_params($database, "SELECT I.oggetto
              FROM inventario AS I
              JOIN oggetto AS O ON I.oggetto = O.nome
              WHERE (O.tipo_oggetto = 0
                    OR O.tipo_oggetto = 1
                    OR O.tipo_oggetto = 2
                    OR O.tipo_oggetto = 3)
                    AND
                      I.personaggio = $1
                    AND
                      NOT((O.tipo_oggetto = 2 OR O.tipo_oggetto = 3) AND equipaggiato)
            ORDER BY O.nome;", $param);
        $sca_control_l = pg_num_rows($sca_control);

        echo "<script type ='text/javascript'>
              var scaControl = '';
              sessionStorage.setItem('control', scaControl);
              sessionStorage.setItem('controll', $sca_control_l);
          </script>";

          while ($control_row = pg_fetch_row($sca_control)) {
            $js_array = json_encode($control_row);
            echo "<script type='text/javascript'>
              addToControl($js_array);
            </script>";
          }



        $poc1_query = pg_query_params($database, "SELECT I.id, I.oggetto
          FROM inventario AS I
          JOIN oggetto AS O ON I.oggetto = O.nome
          WHERE (O.tipo_oggetto = 0
          OR O.tipo_oggetto = 1
          OR O.tipo_oggetto = 2
          OR O.tipo_oggetto = 3)
          AND
            I.personaggio = $1 AND I.in_vendita = 0 AND I.scambia IS NULL
          AND
            NOT((O.tipo_oggetto = 2 OR O.tipo_oggetto = 3) AND equipaggiato)
          ORDER BY I.oggetto;", $param);
        $poc1_length = pg_num_rows($poc1_query);


        echo "<script type ='text/javascript'>
              var poc1List = '';
              sessionStorage.setItem('poc1', poc1List);
              sessionStorage.setItem('poc1Dim', $poc1_length);
          </script>";

        while ($poc1_row = pg_fetch_row($poc1_query)) {
          $js_array_poc1 = json_encode($poc1_row);
          echo "<script type='text/javascript'>
            addToPoc1($js_array_poc1);
          </script>";
        }


        $poc2_query = pg_query($database, "SELECT nome
            FROM oggetto
            WHERE (tipo_oggetto <4)
            ORDER BY nome;");
        $poc2_length = pg_num_rows($poc2_query);


        echo "<script type ='text/javascript'>
              var poc2List = '';
              sessionStorage.setItem('poc2', poc2List);
              sessionStorage.setItem('poc2Dim', $poc2_length);
          </script>";

        while ($poc2_row = pg_fetch_row($poc2_query)) {
          $js_array_poc2 = json_encode($poc2_row);
          echo "<script type='text/javascript'>
            addToPoc2($js_array_poc2);
          </script>";
        }


        $bic_query =  pg_query_params($database, "SELECT I.id, I.oggetto, I.in_vendita
          FROM inventario AS I
          JOIN oggetto AS O ON I.oggetto = O.nome
          WHERE (O.tipo_oggetto = 0
          OR O.tipo_oggetto = 1
          OR O.tipo_oggetto = 2
          OR O.tipo_oggetto = 3)
          AND
            I.personaggio = $1 AND I.in_vendita = 0 AND I.scambia IS NOT NULL
          AND
            NOT((O.tipo_oggetto = 2 OR O.tipo_oggetto = 3) AND equipaggiato)
          ORDER BY I.oggetto;", $param);
        $bic_length = pg_num_rows($bic_query);

        echo "<script type ='text/javascript'>
              var bicList = '';
              sessionStorage.setItem('bic', bicList);
              sessionStorage.setItem('bicDim', $bic_length);
          </script>";

        while ($bic_row = pg_fetch_row($bic_query)) {
          $js_array_bic = json_encode($bic_row);
          echo "<script type='text/javascript'>
            addToBic($js_array_bic);
          </script>";
        }


       echo "<script type ='text/javascript'>
         window.location.href='../scambio.html';
        </script>";
        ?>
  </body>
</html>
