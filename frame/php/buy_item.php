<!DOCTYPE html>
<?php
  session_start();
  ?>
<html>
  <head>
    <meta charset="utf-8" />
    <meta content="Pagina di gioco" />
    <title>Compra</title>
    <link rel="icon" href="/css/image/icona.ico" />
    <link href="/frame/css/inv.css" rel="stylesheet" type="text/css" />
  </head>

  <body>
    <pre>
      <?php
        $param = array($_POST["compra01"], $_SESSION["personaggio"]);
        $database = pg_connect("host=localhost port=5432 dbname=dungeonasdb user=postgres password=postgres")
           or die("Impossibile connettersi.");
        $path_query = pg_query($database, "SET search_path TO dungeonasdb");
        ?>
    </pre>
    <?php

      $mark_query = pg_query_params($database, "SELECT compra($1,$2);", $param);
      $result = pg_fetch_row($mark_query);
      if($result[0]==2){
        echo "<script type ='text/javascript'>
         alert('Non hai abbastanza spazio nello zaino!');
        </script>";
      }
      else if($result[0]==1){
        echo "<script type ='text/javascript'>
         alert('Non hai abbastanza monete!');
        </script>";
      }
      echo "<script type ='text/javascript'>
       window.location.href='upd_mar.php';
      </script>";
      ?>
  </body>
</html>
