<!DOCTYPE html>
<?php
  session_start();
  ?>
<html>
  <head>
    <meta charset="utf-8" />
    <meta content="Pagina di gioco" />
    <title>Battaglia</title>
    <link rel="icon" href="/css/image/icona.ico" />
    <link href="/frame/css/inv.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../javascript/fight3.js"></script>
  </head>
  <body>
    <pre>
      <?php
        $param = array($_SESSION["personaggio"]);
        $database = pg_connect("host=localhost port=5432 dbname=dungeonasdb user=postgres password=postgres")
           or die("Impossibile connettersi.");
        $path_query = pg_query($database, "SET search_path TO dungeonasdb");
        ?>
    </pre>
      <?php
        $player_query = pg_query_params($database, "SELECT P.nome, P.punti_ferita, P.attacco, P.difesa, O.punti_attacco, P.in_vita FROM personaggio P
                                                  JOIN inventario I ON P.id = I.personaggio
                                                  JOIN oggetto O ON I.oggetto = O.nome
                                                  WHERE equipaggiato
                                                  AND tipo_oggetto = 0
                                                  AND P.id = $1;", $param);

        $pg_length = pg_num_rows($player_query);
        if($pg_length == 0){
          $player_query = pg_query_params($database, "SELECT P.nome, P.punti_ferita, P.attacco, P.difesa, O.punti_attacco, P.in_vita FROM personaggio P
                                                    JOIN inventario I ON P.id = I.personaggio
                                                    JOIN oggetto O ON I.oggetto = O.nome
                                                    WHERE tipo_oggetto = 5
                                                    AND P.id = $1;", $param);
        }
        $player = pg_fetch_row($player_query);
        $js_array = json_encode($player);
        echo "<script type ='text/javascript'>
          sessionStorage.setItem('pgFight', $js_array);
         </script>";
        $enemy_query = pg_query_params($database, "SELECT N.nome, C.in_vita, N.attacco, N.difesa, N.danni, N.descrizione, C.id, N.punti_ferita FROM nemico N
                                                   JOIN collocato C ON N.nome = C.nemico
                                                   JOIN personaggio P ON P.id = $1
                                                   WHERE C.stanza = P.stanza ORDER BY C.id;", $param);
        $en_length = pg_num_rows($enemy_query);
        echo "<script type ='text/javascript'>
             var enemyList = '';
             sessionStorage.setItem('enFight', enemyList);
             sessionStorage.setItem('enNum', $en_length);
         </script>";
        if($en_length != 0) {
          while ($enemy = pg_fetch_row($enemy_query)) {
            $js_array = json_encode($enemy);
            echo "<script type ='text/javascript'>
              addToEnemy($js_array);
             </script>";
          }
        }

        $type_query = pg_query_params($database, "SELECT S.tipo_stanza FROM stanza S
                                                 JOIN personaggio P ON P.stanza = S.codice_numerico WHERE P.id = $1;", $param);
        $type_fetch = pg_fetch_row($type_query);
        echo "<script type ='text/javascript'>
             sessionStorage.setItem('type', $type_fetch[0]);
         </script>";

        echo "<script type ='text/javascript'>
         window.location.href='../fight.html';
        </script>";
       ?>
  </body>
</html>
