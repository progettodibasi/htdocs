function setButton() {
  var current = sessionStorage.getItem('pgPos');
  var su = sessionStorage.getItem('nav_su');
  var sx = sessionStorage.getItem('nav_sx');
  var dx = sessionStorage.getItem('nav_dx');
  var giu = sessionStorage.getItem('nav_giu');
  activate("navsu", su, "su");
  activate("navsx", sx, "sx");
  activate("navdx", dx, "dx");
  activate("navgiu", giu, "giu");

  setArray();
}

function activate(id, val, button) {
  if(val != -1 && val != "undefined")
    document.getElementById(id).value = val;
  else
    document.getElementById(button).hidden = true;
}

//MENU OGGETTI RACCOGLIBILI

function addToList(playerTable) {
  var tr = sessionStorage.getItem('roomItem');
  if(tr == null)
    tr = playerTable;
  else
    tr += playerTable;
  tr += "\n";
  sessionStorage.setItem('roomItem', tr);
}

var vettore;

function assignNum(){
  vettore = new Array(sessionStorage.getItem('numItem'));
}

function setArray(){

  if(sessionStorage.getItem('enNum')>0){
    if(sessionStorage.getItem('type')!=3)
      document.getElementById('search').innerHTML = "Devi sconfiggere tutti i nemici prima di poter raccogliere altri oggetti o cercare passaggi/oggetti nascosti!";
    else
      document.getElementById('search').innerHTML = "Devi sconfiggere il boss per terminare la partita!";
  }
  else {

    assignNum();
    var init = sessionStorage.getItem('roomItem');
    init = init.split("\n");
    for(var i=0; i<sessionStorage.getItem('numItem'); i++){
      if(init[i] != ""){
        vettore[i] = init[i].split(",");
        appendButt(vettore[i][1], i);
      }
    }
  }
  document.getElementById('raccogli').disabled = true;
}

var count = 0;

function appendButt(itemName, num){
  var btn = document.createElement("BUTTON");
  if(vettore[num][2] == "")
    var t = document.createTextNode(itemName);
  else
    var t = document.createTextNode(itemName+" ("+vettore[num][2]+")");
  btn.value = num;
  btn.style.width="100%";
  btn.onclick = selectItem;
  btn.appendChild(t);
  btn.id = "bottone" + count;
  btn.disabled = false;
  document.getElementById('scroll').append(btn);
  count++;
}

function selectItem(ev){
  document.getElementById('raccogli').disabled = false;
  num = ev.target.value;
  document.getElementById("itemId").value = vettore[num][0];
  document.getElementById("itemNome").value = vettore[num][1];
  if(vettore[num][2] == "")
    document.getElementById("itemQnt").value = -1;
  else
    document.getElementById("itemQnt").value = vettore[num][2];
  for(var n = 0; n<count; n++){
    var stringa  = "bottone" + n;
    document.getElementById(stringa).disabled = false;
  }
  ev.target.disabled=true;
}
