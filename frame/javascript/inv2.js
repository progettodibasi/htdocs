function addToInv(inventoryTable) {
  var tr = sessionStorage.getItem('inventory');
  if(tr == null)
    tr = inventoryTable;
  else
    tr += inventoryTable;
  tr += "\n";
  sessionStorage.setItem('inventory', tr);
}

var vettore;

function assignNum(){
  vettore = new Array(sessionStorage.getItem('invDim'));
}


function setArray(){
  assignNum();
  var init = sessionStorage.getItem('inventory');
  init = init.split("\n");
  for(var i=0; i<sessionStorage.getItem('invDim'); i++){
    if(init[i] != ""){
      vettore[i] = init[i].split(",");
      if(vettore[i][7] != 4){
        if(vettore[i][7] == 2 || vettore[i][7] == 3){
          if(vettore[i][9] == 'f')
            appendButt(vettore[i][0], i);
        }
        else
          appendButt(vettore[i][0], i);
      }
      else
        document.getElementById("money").innerHTML += "</br>"+vettore[i][1];
    }
  }
}

var count = 0;
var atk;
var def;

function appendButt(objName, num){
  var btn = document.createElement("BUTTON");
  var eq = " ";
  if(vettore[num][9] == 't'){
    eq = " * ";
    if(vettore[num][7] == 0)
      atk = vettore[num];
    else if(vettore[num][7] == 1)
      def = vettore[num];
  }
  var t = document.createTextNode(eq+objName+" ");
  btn.value = num;
  btn.style.width="100%";
  btn.style.height="100%";
  btn.onclick = showSelectedObj;
  btn.appendChild(t);
  btn.id = "bottone" + count;
  var pos = "el"+count;
  document.getElementById(pos).append(btn);
  count++;
}

function showDiff(id, type, num, index){
  var change;
  if(type == 0 && atk != null){
    change = atk;
  }
  else if(type == 1 && def != null)
    change = def;
  else{
    document.getElementById(id).innerHTML = "";
    return;
  }
  var diff = vettore[num][index] - change[index];
  document.getElementById(id).innerHTML = "("+diff+")";
}

function showSelectedObj(ev) {
  num = ev.target.value;
  document.getElementById("invId0").value = vettore[num][10];
  document.getElementById("invId2").value = vettore[num][10];
  document.getElementById("stat4").innerHTML = "";
  document.getElementById("stat14").innerHTML = "";
  document.getElementById("stat24").innerHTML = "";
  document.getElementById("descr").innerHTML = vettore[num][2];
  document.getElementById("stat0").innerHTML = "ATK:"
  document.getElementById("stat10").innerHTML = vettore[num][3];
  showDiff("stat20", vettore[num][7], num, 3);
  document.getElementById("stat1").innerHTML = "DEF:"
  document.getElementById("stat11").innerHTML = vettore[num][4];
  showDiff("stat21", vettore[num][7], num, 4);
  document.getElementById("stat2").innerHTML = "PER:"
  document.getElementById("stat12").innerHTML = vettore[num][5];
  showDiff("stat22", vettore[num][7], num, 5);
  document.getElementById("stat3").innerHTML = " PF:"
  document.getElementById("stat13").innerHTML = vettore[num][6];
  showDiff("stat23", vettore[num][7], num, 6);
  if(vettore[num][8] != ""){
    document.getElementById("stat4").innerHTML = " PA:"
    document.getElementById("stat14").innerHTML = vettore[num][8];
    showDiff("stat24", vettore[num][7], num, 8);
  }
  document.getElementById("butt1").disabled = false;
  document.getElementById("butt2").disabled = false;

  switch (vettore[num][7]) {
    case '0':
    case '1':
      if(vettore[num][9] == 't')
        document.getElementById("butt1").value = "disequipaggia";
      else
        document.getElementById("butt1").value = "equipaggia";
        break;
    case '2':
    case '3':
        document.getElementById("butt1").value = "usa";
        break;
    default:
        document.getElementById("butt1").value = "usa/equipaggia";
  }

  for(var n = 0; n<count; n++){
    var stringa  = "bottone" + n;
    document.getElementById(stringa).disabled = false;
  }
  ev.target.disabled=true;
}
