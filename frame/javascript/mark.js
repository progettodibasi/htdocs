function checkValore(){
  var vendita = document.getElementById('prezzo').value;
  if(vendita!="" && !isNaN(parseInt(vendita)) && isFinite(vendita) && vendita>=0){
    document.getElementById('markId0').value = parseInt(document.getElementById('prezzo').value);
    document.getElementById('vendi').disabled = false;
  }
  else{
    document.getElementById('vendi').disabled = true;
  }
}

function change1(){
  window.location.href = "php/upd_mar.php";
}

function change2(){
  window.location.href = "php/upd_scam.php";
}

function addToMar(marketTable) {
  var tr = sessionStorage.getItem('market');
  if(tr == null)
    tr = marketTable;
  else
    tr += marketTable;
  tr += "\n";
  sessionStorage.setItem('market', tr);
}

function addToSell(sellTable) {
  var tr1 = sessionStorage.getItem('sell');
  if(tr1 == null)
    tr1 = sellTable;
  else
    tr1 += sellTable;
  tr1 += "\n";
  sessionStorage.setItem('sell', tr1);
}

function addToBack(backTable) {
  var tr2 = sessionStorage.getItem('back');
  if(tr2 == null)
    tr2 = backTable;
  else
    tr2 += backTable;
  tr2 += "\n";
  sessionStorage.setItem('back', tr2);
}


var vettore;
var vettore2;
var vettore3;

function assignNum(){
  vettore = new Array(sessionStorage.getItem('marDim'));
  vettore2 =  new Array(sessionStorage.getItem('sellDim'));
  vettore3 =  new Array(sessionStorage.getItem('backDim'));
}

var s = true;
function setArray(){
  assignNum();
  var init = sessionStorage.getItem('market');
  var init2 = sessionStorage.getItem('sell');
  var init3 = sessionStorage.getItem('back');

  init = init.split("\n");
  init2 = init2.split("\n");
  init3 = init3.split("\n");


  for(var i=0; i<sessionStorage.getItem('marDim'); i++){
    if(init[i] != ""){
      vettore[i] = init[i].split(",");
        if(vettore[i][2]>0){
          s = false;
          appendButt(vettore[i][1], i, vettore[i][2]);
        }
        else{
          s = true;
          appendButt(vettore[i][1], i, vettore[i][3]);
        }
    }
  }

  for(var i=0; i<sessionStorage.getItem('sellDim'); i++){
    if(init2[i] != ""){
      vettore2[i] = init2[i].split(",");
      dropList(vettore2[i][1], i, vettore2[i][0]);
    }
  }

  for(var i=0; i<sessionStorage.getItem('backDim'); i++){
    if(init3[i] != ""){
      vettore3[i] = init3[i].split(",");
      dropListBack(vettore3[i][1], i, vettore3[i][0], vettore3[i][2]);
    }
  }
}


function dropList(objName, num1, id){
  var drop = "drop" + num1;
  document.getElementById(drop).value = id;
  document.getElementById(drop).hidden = false;
  document.getElementById(drop).innerHTML = objName;
}

function dropListBack(objName, num1, id, scambio){
  var drop = "dropp" + num1;
  document.getElementById(drop).value = id;
  document.getElementById(drop).hidden = false;
  document.getElementById(drop).innerHTML = objName  + " [$" + scambio + "]";
}



var count = 0;

function appendButt(objName, num, scambio){
  var btn = document.createElement("BUTTON");
  if(s == false)
    var t = document.createTextNode(objName+" [$" + scambio + "]");
  if(s == true)
    var t = document.createTextNode(objName+" [" + scambio + "]");
  btn.value = num;
  btn.style.width="100%";
  btn.onclick = showSelectedObj;
  btn.appendChild(t);
  btn.id = "bottone" + count;
  document.getElementById('scroll1').append(btn);
  count++;
  document.getElementById("mon").innerHTML = sessionStorage.getItem('moneteMark');

}

function showSelectedObj(ev) {
  num = ev.target.value;
  document.getElementById("compra01").value = vettore[num][0];
  // document.getElementById("invId1").value = vettore[num][9];


  document.getElementById("stat4").innerHTML = "";
  document.getElementById("stat14").innerHTML = "";
  document.getElementById("stat24").innerHTML = "";
  // document.getElementById("descr").innerHTML = vettore[num][2];
  document.getElementById("stat0").innerHTML = "ATK:"
  document.getElementById("stat10").innerHTML = vettore[num][5];
  // showDiff("stat20", vettore[num][7], num, 3);
  document.getElementById("stat1").innerHTML = "DEF:"
  document.getElementById("stat11").innerHTML = vettore[num][6];
  // showDiff("stat21", vettore[num][7], num, 4);
  document.getElementById("stat2").innerHTML = "PER:"
  document.getElementById("stat12").innerHTML = vettore[num][7];
  // showDiff("stat22", vettore[num][7], num, 5);
  document.getElementById("stat3").innerHTML = " PF:"
  document.getElementById("stat13").innerHTML = vettore[num][8];
  // showDiff("stat23", vettore[num][7], num, 6);
  if(vettore[num][10] != ""){
    document.getElementById("stat4").innerHTML = " PA:"
    document.getElementById("stat14").innerHTML = vettore[num][10];
    // showDiff("stat24", vettore[num][7], num, 8);
  }
  // document.getElementById("butt1").disabled = false;
  // document.getElementById("butt2").disabled = false;
  //
  // switch (vettore[num][7]) {
  //   case '0':
  //   case '1':
  //     if(vettore[num][9] == 't')
  //       document.getElementById("butt1").value = "disequipaggia";
  //     else
  //       document.getElementById("butt1").value = "equipaggia";
  //       break;
  //   case '2':
  //       document.getElementById("butt1").value = "usa";
  //       break;
  //   default:
  //       document.getElementById("butt1").value = "usa/equipaggia";
  // }

  for(var n = 0; n<count; n++){
    var stringa  = "bottone" + n;
    document.getElementById(stringa).disabled = false;
  }
  document.getElementById("compra").disabled = false;
  ev.target.disabled=true;
}


//SCAMBIA//

function addToSca(exchangeTable) {
  var tr4 = sessionStorage.getItem('scambio');
  if(tr4 == null)
    tr4 = exchangeTable;
  else
    tr4 += exchangeTable;
  tr4 += "\n";
  sessionStorage.setItem('scambio', tr4);
}

function addToControl(controlTable) {
  var tr5 = sessionStorage.getItem('control');
  if(tr5 == null)
    tr5 = controlTable;
  else
    tr5 += controlTable;
  tr5 += "\n";
  sessionStorage.setItem('control', tr5);
}

function addToPoc1(poc1Table) {
  var tr6 = sessionStorage.getItem('poc1');
  if(tr6 == null)
    tr6 = poc1Table;
  else
    tr6 += poc1Table;
  tr6 += "\n";
  sessionStorage.setItem('poc1', tr6);
}

function addToPoc2(poc2Table) {
  var tr7 = sessionStorage.getItem('poc2');
  if(tr7 == null)
    tr7 = poc2Table;
  else
    tr7 += poc2Table;
  tr7 += "\n";
  sessionStorage.setItem('poc2', tr7);
}

function addToBic(bicTable) {
  var tr8 = sessionStorage.getItem('bic');
  if(tr8 == null)
    tr8 = bicTable;
  else
    tr8 += bicTable;
  tr8 += "\n";
  sessionStorage.setItem('bic', tr8);
}


var vettore4;
var controllo;
var vettore6
var vettore7;
var vettore8;

function assignNum2(){
  vettore4 = new Array(sessionStorage.getItem('scaDim'));
  controllo = new Array(sessionStorage.getItem('controll'));
  vettore6 =  new Array(sessionStorage.getItem('poc1Dim'));
  vettore7 =  new Array(sessionStorage.getItem('poc2Dim'));
  vettore8 =  new Array(sessionStorage.getItem('bicDim'));
}



var s = true;
function setArray2(){
  assignNum2();
  var init4 = sessionStorage.getItem('scambio');
  var init5 = sessionStorage.getItem('control');
  var init6 = sessionStorage.getItem('poc1');
  var init7 = sessionStorage.getItem('poc2');
  var init8 = sessionStorage.getItem('bic');

  init4 = init4.split("\n");
  init5 = init5.split("\n");
  init6 = init6.split("\n");
  init7 = init7.split("\n");
  init8 = init8.split("\n");


  for(var i=0; i<sessionStorage.getItem('scaDim'); i++){
    if(init4[i] != ""){
      vettore4[i] = init4[i].split(",");
        if(vettore4[i][2]>0){
          s = false;
          appendButt2(vettore4[i][1], i, vettore4[i][2]);
        }
        else{
          s = true;
          appendButt2(vettore4[i][1], i, vettore4[i][3]);
        }
    }
  }

  for(var i=0; i<sessionStorage.getItem('controll'); i++){
    if(init5[i] != ""){
    controllo[i] = init5[i].split(",");
    }
  }

  for(var i=0; i<sessionStorage.getItem('poc1Dim'); i++){
    if(init6[i] != ""){
      vettore6[i] = init6[i].split(",");
      dropList(vettore6[i][1], i, vettore6[i][0]);
    }
  }

  for(var i=0; i<sessionStorage.getItem('poc2Dim'); i++){
    if(init7[i] != ""){
      vettore7[i] = init7[i].split(",");
      var opzione = document.createElement('option');
      opzione.value = vettore7[i][0];
      opzione.innerHTML = vettore7[i][0];
      document.getElementById("poc2").add(opzione);
    }
  }


  for(var i=0; i<sessionStorage.getItem('bicDim'); i++){
    if(init8[i] != ""){
      vettore8[i] = init8[i].split(",");
      dropListBack2(vettore8[i][1], i, vettore8[i][0], vettore8[i][2]);
    }
  }
}

function dropListBack2(objName, num1, id, scambio){
  var drop = "dropp" + num1;
  document.getElementById(drop).value = id;
  document.getElementById(drop).hidden = false;
  document.getElementById(drop).innerHTML = objName;
}


var count2 = 0;

function appendButt2(objName, num, scambio){
  var btn = document.createElement("BUTTON");
  if(s == false)
    var t = document.createTextNode(objName+" [$" + scambio + "]");
  if(s == true)
    var t = document.createTextNode(objName+" [con " + scambio + "]");
  btn.value = num;
  btn.style.width="100%";
  btn.onclick = showSelectedObj2;
  btn.appendChild(t);
  btn.id = "bottone2" + count2;
  document.getElementById('scroll3').append(btn);
  count2++;
  // document.getElementById("mon").innerHTML = sessionStorage.getItem('moneteMark');
}

function showSelectedObj2(ev) {
  num = ev.target.value;
  document.getElementById("scambia01").value = vettore4[num][0];



  document.getElementById("stat4").innerHTML = "";
  document.getElementById("stat14").innerHTML = "";
  document.getElementById("stat24").innerHTML = "";
  // document.getElementById("descr").innerHTML = vettore[num][2];
  document.getElementById("stat0").innerHTML = "ATK:"
  document.getElementById("stat10").innerHTML = vettore4[num][5];
  // showDiff("stat20", vettore[num][7], num, 3);
  document.getElementById("stat1").innerHTML = "DEF:"
  document.getElementById("stat11").innerHTML = vettore4[num][6];
  // showDiff("stat21", vettore[num][7], num, 4);
  document.getElementById("stat2").innerHTML = "PER:"
  document.getElementById("stat12").innerHTML = vettore4[num][7];
  // showDiff("stat22", vettore[num][7], num, 5);
  document.getElementById("stat3").innerHTML = " PF:"
  document.getElementById("stat13").innerHTML = vettore4[num][8];
  // showDiff("stat23", vettore[num][7], num, 6);
  if(vettore4[num][10] != ""){
    document.getElementById("stat4").innerHTML = " PA:"
    document.getElementById("stat14").innerHTML = vettore4[num][10];
    // showDiff("stat24", vettore[num][7], num, 8);
  }
  // document.getElementById("butt1").disabled = false;
  // document.getElementById("butt2").disabled = false;
  //
  // switch (vettore[num][7]) {
  //   case '0':
  //   case '1':
  //     if(vettore[num][9] == 't')
  //       document.getElementById("butt1").value = "disequipaggia";
  //     else
  //       document.getElementById("butt1").value = "equipaggia";
  //       break;
  //   case '2':
  //       document.getElementById("butt1").value = "usa";
  //       break;
  //   default:
  //       document.getElementById("butt1").value = "usa/equipaggia";
  // }

  for(var n = 0; n<count2; n++){
    var stringa  = "bottone2" + n;
    document.getElementById(stringa).disabled = false;
  }
  document.getElementById("scambio").disabled = true;
  document.getElementById("scambio").value = "OGGETTO NON POSSEDUTO";
  ev.target.disabled=true;
  var posseduto = false;
  for(var y = 0; y<sessionStorage.getItem('controll'); y++){
    if(controllo[y][0]==vettore4[num][3]){
      posseduto = true;
    }
  }

  if(posseduto){
    document.getElementById("scambio").disabled = false;
    document.getElementById("scambio").value = "SCAMBIA";
  }
  else{
    document.getElementById("scambio").disabled = true;
    document.getElementById("scambio").value = "OGGETTO NON POSSEDUTO";
  }

}
