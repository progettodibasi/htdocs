function change(address){
  document.getElementById("fdx").src = address;
}

function addToEnemy(enemyRow) {
  var tr = sessionStorage.getItem('enFight');
  if(tr == null)
    tr = enemyRow;
  else
    tr += enemyRow;
  tr += "\n";
  sessionStorage.setItem('enFight', tr);
}

var listaNemici;
var personaggio;

function assignNum(){
  listaNemici = new Array(sessionStorage.getItem('enNum'));
}

function setArray(){
  if(sessionStorage.getItem('enNum')==0 && sessionStorage.getItem('type')==3){
    window.top.location.href='../win.html';
  }
  var init;
  if(sessionStorage.getItem('enNum')!=0){
    assignNum();
    init = sessionStorage.getItem('enFight');
    init = init.split("\n");
    for(var i=0; i<sessionStorage.getItem('enNum'); i++)
      listaNemici[i] = init[i].split(",");
  }
  init = sessionStorage.getItem('pgFight');
  personaggio = init.split(",");
  if(personaggio[5]<=0){
    window.top.location.href='../game_over.html';
  }
  showPlayer();
  for(var i=0; i<3; i++){
    if(sessionStorage.getItem('enNum')!=0)
      showEnemy(listaNemici[i], i);
    else
      showEnemy(null, i);
  }
  showLog();
}

function showPlayer() {
  document.getElementById("pg_name").innerHTML = personaggio[0];
  document.getElementById("pg_pf").max = Number(personaggio[1]);
  document.getElementById("pg_pf").value = Number(personaggio[5]);
  document.getElementById("pg_atk").innerHTML = personaggio[2];
  document.getElementById("pg_def").innerHTML = personaggio[3];
  document.getElementById("pg_dmg").innerHTML = personaggio[4];
}

function showEnemy(enemy, num) {
  if(enemy == null)
    document.getElementById("enemy"+num).hidden = true;
  else{
    document.getElementById("en"+num+"_name").innerHTML = enemy[0];
    document.getElementById("en"+num+"_pf").max = Number(enemy[7]);
    document.getElementById("en"+num+"_pf").value = Number(enemy[1]);
    document.getElementById("en"+num+"_atk").innerHTML = enemy[2];
    document.getElementById("en"+num+"_def").innerHTML = enemy[3];
    document.getElementById("en"+num+"_dmg").innerHTML = enemy[4];
    document.getElementById("enemy"+num).title = enemy[5];
    document.getElementById("hidden"+num).value = enemy[6];
  }
}

function showLog() {
  var tr = sessionStorage.getItem('logger');
  var logger = tr.substring(1, tr.length-1).split(",");

  if(logger[1] != 0)
    document.getElementById("log").innerHTML += logger[0]+" subisce "+logger[1]+" danni. ";
  if(logger[3] != 0){
    document.getElementById("log").innerHTML += logger[2]+" subisce "+personaggio[4]+" danni";
    if(logger[3] == -1)
      document.getElementById("log").innerHTML += " ed è stato sconfitto";
    document.getElementById("log").innerHTML += ".";
  }
  sessionStorage.setItem('logger', '0,0,0,0');
}
