function mapInit(mapTable) {
  var tr = sessionStorage.getItem('map');
  if(tr == null)
    tr = mapTable;
  else
    tr += mapTable;
  tr += "\n";
  sessionStorage.setItem('map', tr);
}

var stanze = new Array(21);

function setArray() {
  var init = sessionStorage.getItem('map');
  init = init.split("\n");
  for(var i=0; i<21; i++)
    stanze[i] = init[i].split(",");
  drawMap();

}

function drawMap(){
    var pos = [3, 3];
    var rotation = 0;
    document.getElementById(""+pos[0]+pos[1]).value = stanze[0][0];
    if(stanze[0][0] == sessionStorage.getItem('pgPos'))
      document.getElementById(""+pos[0]+pos[1]).className = "current"+stanze[0][1];
    else
      document.getElementById(""+pos[0]+pos[1]).className = "type"+stanze[0][1];
    for(var i = 1; i<5; i++){
      rotation = placeFirstRound(rotation, pos, i);
    }
    rotation = 0;
    for(var i = 5; i<13; i=i+2){
      rotation = placeSecondRound(rotation, pos, i);
    }
    rotation = 0;
    var rotation2 = 0;
    var rotation3 = 1;
    for(var i = 13; i<21; i++){
      pos = move(rotation, pos);
      for(var j = 0; j<2; j++){
        var placed = false;
        pos = move(rotation2%4, pos);
        placeFirstRound(rotation3, pos, i);
        pos = move((rotation2+2)%4, pos);
        rotation2 = (rotation2+1);
        i++;
      }
      i--;
      rotation2 = (rotation2-1)%4;
      pos = move((rotation+2)%4, pos);
      rotation = (rotation+1)%4;
      rotation3 = (rotation3+1)%4;
    }
    setNavigation();
    //window.top.frames[1].location.reload();
}

function up(pos){ pos[0]--; return pos; }
function down(pos) { pos[0]++; return pos; }
function left(pos) { pos[1]--; return pos; }
function right(pos) { pos[1]++; return pos; }
function isEmpty(pos) {
  if (!document.getElementById(""+pos[0]+pos[1]).value)
    return true;
  else
    return false;
}
function move(rotation, pos) {
  switch (rotation) {
    case 0: return up(pos);
    case 1: return right(pos);
    case 2: return down(pos);
    case 3: return left(pos);
  }
}
function placeFirstRound(rotation, pos, i) {
  var placed = false;
  while(!placed){
    var prev = [pos[0], pos[1]];
    pos = move(rotation, pos);
    var next = [pos[0], pos[1]];
    if(isEmpty(pos)){
      if(stanze[i][0]){
        document.getElementById(""+pos[0]+pos[1]).value = -1;
        // document.getElementById(""+pos[0]+pos[1]).innerHTML = stanze[i][0];
        if(stanze[i][2] != 0){
          document.getElementById(""+pos[0]+pos[1]).value = stanze[i][0];
          if(stanze[i][0] == sessionStorage.getItem('pgPos'))
            document.getElementById(""+pos[0]+pos[1]).className = "current"+stanze[i][1];
          else
            document.getElementById(""+pos[0]+pos[1]).className = "type"+stanze[i][1]+stanze[i][2];
          drawCorridor(prev, next);
        }
      }
      else{
        document.getElementById(""+pos[0]+pos[1]).value = -1;
      }
      placed = true;
    }
    pos = move((rotation+2)%4, pos);
    rotation = (rotation+1)%4;
  }
  return rotation;
}
function placeSecondRound(rotation, pos, i) {
  var rotation2 = 0;
  if(rotation==3)
    rotation2++;
  pos = move(rotation, pos);
  rotation2 = placeFirstRound(rotation2, pos, i);
  i++;
  rotation2 = placeFirstRound(rotation2, pos, i);
  pos = move((rotation+2)%4, pos);
  rotation = (rotation+1)%4;
  return rotation;
}
function drawCorridor(prev, next) {
  var let = ["a", "b", "c", "d"];
  if(prev[0] == next[0]){
    if(prev[1] < next[1])
      document.getElementById(""+next[0]+let[prev[1]-1]).className = "corridor1";
    else
      document.getElementById(""+next[0]+let[next[1]-1]).className = "corridor1";
  }
  else{
    if(prev[0] < next[0])
      document.getElementById(""+let[prev[0]-1]+next[1]).className = "corridor1";
    else
      document.getElementById(""+let[next[0]-1]+next[1]).className = "corridor1";
  }
}

function setNavigation() {
  var current = sessionStorage.getItem('pgPos');
  for(var i=1; i<6; i++){
    for(var j=1; j<6; j++){
      if(document.getElementById(""+i+j).value == current)
        current = [i,j];
    }
  }
  setPos("nav_su", current, -1, 0);
  setPos("nav_sx", current, 0, -1);
  setPos("nav_dx", current, 0, 1);
  setPos("nav_giu", current, 1, 0);
}

function setPos(id, position, i, j) {
  var value;
  if(document.getElementById(""+(position[0]+i)+(position[1]+j)) && checkCorridor(position, [position[0]+i, position[1]+j]))
    value = document.getElementById(""+(position[0]+i)+(position[1]+j)).value;
  else
    value = -1;
  sessionStorage.setItem(id, value);
}

function checkCorridor(prev, next) {      //3,5 -> 3,4
  var let = ["a", "b", "c", "d"];
  if(prev[0] == next[0]){
    if(prev[1] < next[1]){
      if(document.getElementById(""+next[0]+let[prev[1]-1]).className == "corridor1")
        return true;
    }
    else{
      if(document.getElementById(""+next[0]+let[next[1]-1]).className == "corridor1")
        return true;
    }
  }
  else{
    if(prev[0] < next[0]){
      if(document.getElementById(""+let[prev[0]-1]+next[1]).className == "corridor1")
        return true;
    }
    else{
      if(document.getElementById(""+let[next[0]-1]+next[1]).className == "corridor1")
        return true;
    }
  }
  return false;
}
