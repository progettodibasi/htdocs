create database dungeonAsDb;
create schema dungeonAsDb;
SET search_path TO dungeonasdb;

create sequence name_num_seq
;

create sequence dungeon
;

create table utente
(
	email varchar(50) not null
		constraint utente_pkey
			primary key
		constraint utente_email_check
			check ((email)::text ~ similar_escape('_%@_%._%'::text, NULL::text)),
	password varchar(32) not null,
	nome varchar(20) default ('utente'::text || nextval('name_num_seq'::regclass)) not null
		constraint utente_nome_key
			unique
)
;

create table stanza
(
	codice_numerico serial not null
		constraint stanza_pkey
			primary key,
	descrizione varchar(100) not null,
	tipo_stanza integer not null,
	status integer default 0 not null,
	dungeon bigint not null
)
;

create table personaggio
(
	id serial not null
		constraint personaggio_pkey
			primary key,
	utente varchar(50) not null
		constraint personaggio_utente_fkey
			references utente
				on update cascade on delete cascade,
	stanza integer
		constraint stanza_fk
			references stanza
				on update cascade on delete set null,
	punti_esperienza bigint default 0,
	nome varchar(20) not null,
	descrizione varchar(1000),
	forza integer not null,
	intelligenza integer not null,
	costituzione integer not null,
	agilita integer not null,
	attacco integer not null,
	difesa integer not null,
	percezione integer not null,
	punti_ferita integer not null,
	in_vita integer not null
)
;

create table passaggio
(
	da_stanza integer not null
		constraint passaggio_da_stanza_fkey
			references stanza
				on update cascade on delete cascade,
	a_stanza integer not null
		constraint passaggio_a_stanza_fkey
			references stanza
				on update cascade on delete cascade,
	constraint passaggio_pkey
		primary key (da_stanza, a_stanza),
	constraint passaggio_check
		check (da_stanza <> a_stanza)
)
;

create table nemico
(
	nome varchar(20) not null
		constraint nemico_pkey
			primary key,
	descrizione varchar(100) not null,
	attacco integer not null,
	difesa integer not null,
	danni integer not null,
	punti_ferita integer not null
)
;

create table collocato
(
	id serial not null
		constraint collocato_pkey
			primary key,
	stanza integer not null
		constraint collocato_stanza_fkey
			references stanza
				on update cascade on delete cascade,
	nemico varchar(20) not null
		constraint collocato_nemico_fkey
			references nemico
				on update cascade on delete cascade,
	in_vita integer not null
)
;

create table oggetto
(
	nome varchar(20) not null
		constraint oggetto_pkey
			primary key,
	descrizione varchar(100),
	attacco integer not null
		constraint oggetto_attacco_check
			check ((attacco >= '-6'::integer) AND (attacco <= 6)),
	difesa integer not null
		constraint oggetto_difesa_check
			check ((difesa >= '-6'::integer) AND (difesa <= 6)),
	percezione integer not null
		constraint oggetto_percezione_check
			check ((percezione >= '-6'::integer) AND (percezione <= 6)),
	punti_ferita integer not null
		constraint oggetto_punti_ferita_check
			check ((punti_ferita >= '-6'::integer) AND (punti_ferita <= 6)),
	tipo_oggetto integer not null,
	punti_attacco integer
		constraint oggetto_punti_attacco_check
			check (punti_attacco > 0)
)
;

create table posizionato
(
	id serial not null
		constraint posizionato_pkey
			primary key,
	stanza integer not null
		constraint posizionato_stanza_fkey
			references stanza
				on update cascade on delete cascade,
	oggetto varchar(20) not null
		constraint posizionato_oggetto_fkey
			references oggetto
				on update cascade on delete cascade,
	visibilita boolean not null,
	quantita integer
)
;

create table inventario
(
	id serial not null
		constraint inventario_pkey
			primary key,
	personaggio integer not null
		constraint inventario_personaggio_fkey
			references personaggio
				on update cascade on delete cascade,
	oggetto varchar(20) not null
		constraint inventario_oggetto_fkey
			references oggetto
				on update cascade on delete cascade,
	equipaggiato boolean default false not null,
	in_vendita integer default 0 not null,
	scambia varchar(20)
		constraint inventario_scambia_fkey
			references oggetto
				on update cascade on delete cascade,
	quantita integer
)
;

create function fill_room() returns trigger
	language plpgsql
as $$
DECLARE
      hide_coin INTEGER;
      item dungeonasdb.oggetto%ROWTYPE;
      max INTEGER;
      enemy dungeonasdb.nemico%ROWTYPE;
      dmg_limit INTEGER;
  BEGIN
    IF NEW.status <> 2 THEN  RETURN NEW; END IF;
    IF (NEW.tipo_stanza=0) OR OLD.status=2 THEN RETURN NEW; END IF;
    IF(NEW.tipo_stanza=3) THEN
      SELECT * FROM dungeonasdb.nemico WHERE punti_ferita>15 ORDER BY random() LIMIT 1 INTO enemy;
      INSERT INTO dungeonasdb.collocato(stanza, nemico, in_vita)
        VALUES(NEW.codice_numerico, enemy.nome, enemy.punti_ferita);
    RETURN NEW;
    END IF;
    --.RIEMPI OGGETTI
    --.da 0 a 5 oggetti
    SELECT(CAST(random()*100 AS INTEGER)%100) INTO max; --.da 0 a 99
    IF max>=0  AND max<=14 THEN max = 0;
    ELSEIF max>=15 AND max<=34 THEN max = 1;
    ELSEIF max>=35 AND max<=69 THEN max = 2;
    ELSEIF max>=70 AND max<=84 THEN max = 3;
    ELSEIF max>=85 AND max<=94 THEN max = 4;
    ELSEIF max>=95 AND max<=99 THEN max = 5;
    END IF;
    FOR i in 0..max LOOP
      SELECT(CAST(random()*11 AS INTEGER)%2) INTO hide_coin;
      SELECT * FROM dungeonasdb.oggetto WHERE tipo_oggetto < 4 ORDER BY random() LIMIT 1 INTO item;
      IF hide_coin=1 THEN
        INSERT INTO dungeonasdb.posizionato(stanza, oggetto, visibilita)
        VALUES(NEW.codice_numerico, item.nome, TRUE);
      ELSE
        INSERT INTO dungeonasdb.posizionato(stanza, oggetto, visibilita)
        VALUES(NEW.codice_numerico, item.nome, FALSE);
      END IF;
    END LOOP;
    --.da 0 a 10 monete
    SELECT(CAST(random()*11 AS INTEGER)%2) INTO hide_coin;
    IF hide_coin=1 THEN
      SELECT(CAST(random()*11 AS INTEGER)%10)+1 INTO max;
      INSERT INTO dungeonasdb.posizionato(stanza, oggetto, visibilita, quantita)
        VALUES(NEW.codice_numerico, 'moneta', TRUE, max);
    END IF;
    --.RIEMPI NEMICI
    --.stanza finale da fare
    --.random da 1 a 3 nemici
    SELECT(CAST(random()*11 AS INTEGER)%3)+1 INTO max;
    SELECT punti_ferita FROM dungeonasdb.personaggio WHERE stanza = NEW.codice_numerico INTO dmg_limit;
    FOR i in 1..max LOOP
      SELECT * FROM dungeonasdb.nemico WHERE danni<(dmg_limit/2) AND punti_ferita <=15 ORDER BY random() LIMIT 1 INTO enemy;
      IF enemy ISNULL THEN RETURN NEW; END IF;
      INSERT INTO dungeonasdb.collocato(stanza, nemico, in_vita)
            VALUES(NEW.codice_numerico, enemy.nome, enemy.punti_ferita);
      dmg_limit = dmg_limit - enemy.danni;
    END LOOP;
    RETURN new;

  END;
$$
;

create trigger fill_room_trigger
	before update OF status
	on stanza
	for each row
	execute procedure fill_room()
;

create function add_exp_room() returns trigger
	language plpgsql
as $$
BEGIN
    IF NEW.tipo_stanza <> 0 AND NEW.status = 2 AND NEW.status <> OLD.status THEN
      UPDATE dungeonasdb.personaggio P SET punti_esperienza=punti_esperienza+10
      WHERE P.stanza = NEW.codice_numerico;
    END IF;
    RETURN NEW;
  END;
$$
;

create trigger add_exp_room_trigger
	after update OF status
	on stanza
	for each row
	execute procedure add_exp_room()
;


create function room_status_upd() returns trigger
	language plpgsql
as $$
BEGIN
    UPDATE dungeonasdb.stanza
      SET status = 2
    WHERE NEW.stanza = stanza.codice_numerico;

    UPDATE dungeonasdb.stanza
      SET status = 1
      FROM dungeonasdb.passaggio P
    WHERE stanza.codice_numerico = P.a_stanza
      AND NEW.stanza = P.da_stanza
      AND stanza.status = 0
      AND stanza.tipo_stanza <> 2;

    RETURN NEW;
  END;
$$
;

create trigger room_status_trigger
	after insert or update OF stanza
	on personaggio
	for each row
	execute procedure room_status_upd()
;

create function delete_cons_bonus() returns trigger
	language plpgsql
as $$
DECLARE
    item INTEGER;
  BEGIN
    FOR item IN SELECT id FROM dungeonasdb.inventario I JOIN dungeonasdb.oggetto ON nome = I.oggetto WHERE equipaggiato AND (tipo_oggetto = 2 OR tipo_oggetto = 3) AND personaggio = NEW.id LOOP
      PERFORM dungeonasdb.delete_item(item);
    END LOOP;

    RETURN NEW;
  END;
$$
;

create trigger delete_cons_bonus_trigger
	after update OF stanza
	on personaggio
	for each row
	execute procedure delete_cons_bonus()
;

create function pg_life_check() returns trigger
	language plpgsql
as $$
DECLARE
  var BIGINT;
  cod INTEGER;
BEGIN
    IF(NEW.in_vita > NEW.punti_ferita) THEN
      UPDATE dungeonasdb.personaggio SET in_vita=punti_ferita WHERE id=NEW.id;
    ELSEIF NEW.in_vita <= 0 THEN
      SELECT dungeon FROM dungeonasdb.stanza S
      JOIN dungeonasdb.personaggio P ON P.stanza = S.codice_numerico
      WHERE id=NEW.id
      INTO var;

      FOR cod IN SELECT S.codice_numerico FROM dungeonasdb.stanza S WHERE S.dungeon=var LOOP
        DELETE FROM dungeonasdb.stanza WHERE codice_numerico = cod;
      END LOOP;
    END IF;
    RETURN NEW;
  END;
$$
;

create trigger pg_life_check_trigger
	after update of in_vita
	on personaggio
	for each row
	execute procedure pg_life_check()
;

create function remove_enemy() returns trigger
	language plpgsql
as $$
BEGIN
    IF NEW.in_vita<=0 THEN
      DELETE FROM dungeonasdb.collocato WHERE NEW.id = collocato.id;
      RETURN NULL;
    END IF;
    RETURN NEW;
  END;
$$
;

create trigger remove_enemy_trigger
	after update OF in_vita
	on collocato
	for each row
	execute procedure remove_enemy()
;

create function add_exp_enemy() returns trigger
	language plpgsql
as $$
DECLARE
    dmg INTEGER;
  BEGIN
    SELECT danni FROM dungeonasdb.nemico WHERE OLD.nemico = nome INTO dmg;
    UPDATE dungeonasdb.personaggio P SET punti_esperienza=punti_esperienza+dmg WHERE P.stanza = OLD.stanza;
    RETURN NEW;
  END;
$$
;

create trigger add_exp_enemy_trigger
	after delete
	on collocato
	for each row
	execute procedure add_exp_enemy()
;

create function inv_dim_check() returns trigger
	language plpgsql
as $$
DECLARE
    cur_dim INTEGER;
    max_dim INTEGER;
    tipo INTEGER;
  BEGIN
    SELECT tipo_oggetto FROM oggetto
    JOIN inventario ON oggetto.nome = NEW.oggetto
    INTO tipo;
    IF tipo=4 OR tipo=5 THEN RETURN NEW; END IF;

    SELECT count(*) FROM dungeonasdb.inventario I
    JOIN dungeonasdb.oggetto O ON I.oggetto = O.nome
    WHERE O.tipo_oggetto < 4 AND NEW.personaggio = I.personaggio AND NOT((O.tipo_oggetto = 2 OR O.tipo_oggetto = 3) AND equipaggiato)
    INTO cur_dim;

    cur_dim = cur_dim + 1;
    SELECT costituzione FROM dungeonasdb.personaggio WHERE NEW.personaggio = id INTO max_dim;

    IF (max_dim%2)<>0 THEN max_dim = max_dim/2 + 1;
    ELSE max_dim = max_dim/2;
    END IF;

    IF(cur_dim > max_dim) THEN
      RAISE EXCEPTION 'Non puoi portare altri oggetti!';
    END IF;

    RETURN NEW;
  END;
$$
;

create trigger inv_dim_check_trigger
	before insert
	on inventario
	for each row
	execute procedure inv_dim_check()
;

create function update_coin() returns trigger
	language plpgsql
as $$
DECLARE
    tipo INTEGER;
    id1 INTEGER;
    money dungeonasdb.inventario%ROWTYPE;
  BEGIN
    SELECT tipo_oggetto FROM dungeonasdb.oggetto WHERE nome = NEW.oggetto INTO tipo;

    IF(tipo = 4) THEN
      SELECT id FROM dungeonasdb.inventario WHERE oggetto = 'moneta' AND NEW.personaggio = personaggio INTO id1;
      IF id1 NOTNULL THEN
        UPDATE dungeonasdb.inventario I SET quantita = quantita+NEW.quantita WHERE I.id=id1 RETURNING * INTO money;
        DELETE FROM dungeonasdb.inventario WHERE id=id1;
        RETURN money;
      END IF;
    END IF;

    RETURN NEW;
  END;
$$
;

create trigger update_coin_trigger
	before insert
	on inventario
	for each row
	execute procedure update_coin()
;

create function inv_equip() returns trigger
	language plpgsql
as $$
DECLARE
    tipo INTEGER;
    swap_item RECORD;
  BEGIN
    IF NEW.equipaggiato THEN
      SELECT tipo_oggetto FROM dungeonasdb.oggetto O WHERE O.nome = NEW.oggetto INTO tipo;
      IF tipo < 2 THEN
        SELECT id, personaggio, oggetto FROM dungeonasdb.inventario I
        JOIN dungeonasdb.oggetto O ON O.nome = I.oggetto
        WHERE I.equipaggiato AND I.id <> NEW.id AND O.tipo_oggetto = tipo
        INTO swap_item;
        UPDATE dungeonasdb.inventario SET equipaggiato=FALSE WHERE id = swap_item.id;
      END IF;
    END IF;

    RETURN NEW;
  END;
$$
;

create trigger inv_equip_trigger
	after update OF equipaggiato
	on inventario
	for each row
	execute procedure inv_equip()
;

create function update_stats_pg() returns trigger
	language plpgsql
as $$
DECLARE
    tipo INTEGER;
    bonus RECORD;
  BEGIN
    SELECT tipo_oggetto FROM dungeonasdb.oggetto WHERE nome=NEW.oggetto INTO tipo;
    SELECT attacco, difesa, percezione, punti_ferita FROM dungeonasdb.oggetto
    WHERE nome=NEW.oggetto INTO bonus;
    IF(NEW.equipaggiato) THEN
      IF(tipo<3) THEN
        UPDATE dungeonasdb.personaggio SET attacco      = attacco      + bonus.attacco,
                               difesa       = difesa       + bonus.difesa ,
                               percezione   = percezione   + bonus.percezione,
                               punti_ferita = punti_ferita + bonus.punti_ferita
        WHERE id=NEW.personaggio;
      ELSE
        UPDATE dungeonasdb.personaggio SET in_vita = in_vita + bonus.punti_ferita
        WHERE id=NEW.personaggio;
      END IF;
    ELSE
      IF(tipo<3) THEN
        UPDATE dungeonasdb.personaggio SET attacco      = attacco      - bonus.attacco,
                               difesa       = difesa       - bonus.difesa,
                               percezione   = percezione   - bonus.percezione,
                               punti_ferita = punti_ferita - bonus.punti_ferita
        WHERE id=NEW.personaggio;
      END IF;
    END IF;

    RETURN NEW;
  END;
$$
;

create trigger update_stats_pg_trigger
	after update OF equipaggiato
	on inventario
	for each row
	execute procedure update_stats_pg()
;

create function create_pg(forza1 integer, intelligenza1 integer, costituzione1 integer, agilita1 integer, utentefk character varying, nome1 character varying, descrizione1 character varying) returns void
	language plpgsql
as $$
DECLARE
  attacco1 INTEGER;
  difesa1 INTEGER;
  percezione1 INTEGER;
  punti_ferita1 INTEGER;
  id_pg INTEGER;

BEGIN
        IF((forza1 + agilita1)%2<>0)
          THEN attacco1 = ((forza1 + agilita1)/2)+1;
        ELSE attacco1 = (forza1 + agilita1)/2;
        END IF;

        IF((costituzione1 + agilita1)%2<>0)
          THEN difesa1 = ((costituzione1 + agilita1)/2)+1;
        ELSE difesa1 = (costituzione1 + agilita1)/2;
        END IF;

        percezione1 = intelligenza1;
        punti_ferita1 = costituzione1;
        INSERT INTO personaggio(utente, stanza, nome, descrizione, forza, intelligenza, costituzione, agilita, attacco, difesa, percezione, punti_ferita, in_vita)
        VALUES(utentefk, create_dungeon(), nome1, descrizione1, forza1, intelligenza1, costituzione1, agilita1, attacco1, difesa1, percezione1, punti_ferita1, punti_ferita1)
        RETURNING id INTO id_pg;

        INSERT INTO inventario(personaggio, oggetto)
        VALUES(id_pg ,'spada arrugginita');
        INSERT INTO inventario(personaggio, oggetto)
        VALUES(id_pg ,'razione');
        INSERT INTO inventario(personaggio, oggetto, quantita)
        VALUES(id_pg ,'moneta', 0);
        INSERT INTO inventario(personaggio, oggetto)
        VALUES(id_pg ,'pugni');
END;
$$
;

create function create_dungeon() returns integer
	language plpgsql
as $$
DECLARE
  max INTEGER = 4;
  cn_da INTEGER;
  flag INTEGER[] = '{0, 0, 0}';
BEGIN
  --.radice
  SELECT dungeonasdb.create_room('Stanza iniziale', 0, nextval('dungeon'::REGCLASS)) INTO cn_da;
  --.SETTA IL SEED
  PERFORM setseed(extract(SECONDS FROM current_timestamp)/100);

  SELECT create_dungeon_child(max, cn_da, flag) INTO flag;
  IF flag[2]=0
    THEN UPDATE stanza SET descrizione='stanza finale', tipo_stanza=3 WHERE flag[3]=codice_numerico;
    END IF;

  RETURN cn_da;
END;
$$
;

create function create_dungeon_child(max integer, cn_da integer, fl integer[]) returns integer[]
	language plpgsql
as $$
DECLARE
  num_rm INTEGER;
  type_rm INTEGER;
  cn_a INTEGER;
  flag INTEGER[] = fl;
BEGIN
  IF max<2
    THEN RETURN flag;
  --.Quante stanze collego (da 2 a 4)?
  ELSEIF max=4
    THEN SELECT(CAST(random()*11 AS INTEGER)%3)+2 INTO num_rm;
  --.(da 1 a 2)?
  ELSEIF max=3
    THEN SELECT(CAST(random()*11 AS INTEGER)%2)+1 INTO num_rm;
  --.(da 0 a 1)?
  ELSE
    SELECT(CAST(random()*11 AS INTEGER)%2) INTO num_rm;
    END IF;

  IF num_rm=0
    THEN RETURN flag;
    END IF;

  FOR i in 1..num_rm LOOP
    --.random tipo stanza (1 - normale, 2 - nascosta, 3 - finale)
    IF (flag[1]<>4)AND(flag[2]<>1)AND(max<=3)
      THEN SELECT(CAST(random()*11 AS INTEGER)%3)+1 INTO type_rm;
    ELSEIF (flag[1]<>4)AND(flag[2]=1)
      THEN SELECT(CAST(random()*11 AS INTEGER)%2)+1 INTO type_rm;
    ELSEIF (flag[1]=4)AND(flag[2]<>1)
      THEN SELECT(CAST(random()*11 AS INTEGER)%2)*2+1 INTO type_rm;
    ELSE
      type_rm=1;
      END IF;
    IF type_rm=3
      THEN SELECT dungeonasdb.create_room('Stanza finale', type_rm, currval('dungeon'::REGCLASS)) INTO cn_a;
      ELSE SELECT dungeonasdb.create_room('Stanza '||max||'-'||i, type_rm, currval('dungeon'::REGCLASS)) INTO cn_a;
    END IF;
    PERFORM connect_room(cn_da, cn_a);
    IF type_rm=2
      THEN flag[1]=flag[1]+1;
      END IF;
    IF type_rm=3
      THEN flag[2]=flag[2]+1;
      END IF;
    flag[3] = cn_a;
    IF type_rm<>3
      THEN SELECT create_dungeon_child(max-1, cn_a, flag) INTO flag;
      END IF;
  END LOOP;
  RETURN flag;
END;
$$
;

create function connect_room(da integer, a integer) returns void
	language plpgsql
as $$
BEGIN
  INSERT INTO passaggio(da_stanza, a_stanza) VALUES(da, a);
END;
$$
;

create function delete_item(item integer) returns void
	language plpgsql
as $$
DECLARE
    eq BOOLEAN;
  BEGIN
    SELECT equipaggiato FROM dungeonasdb.inventario WHERE id=item INTO eq;
    IF eq THEN
      UPDATE dungeonasdb.inventario SET equipaggiato=FALSE WHERE id=item;
    END IF;
    DELETE FROM dungeonasdb.inventario WHERE item = id;
  END;
$$
;

create function compra(id_oggetto integer, id_personaggio integer) returns integer
	language plpgsql
as $$
DECLARE
  costituzione INTEGER;
  monete INTEGER;
  prezzo_oggetto INTEGER;
  esito INTEGER;
  oggetti_posseduti INTEGER;
  peso_massimo INTEGER;
  id_venditore INTEGER;
  monete_venditore INTEGER;
  id_mon_c INTEGER;
  id_mon_v INTEGER;
  equipped BOOLEAN;

BEGIN
  esito = 0; /*0 - oggetto acquistato, 1 - non abbastanza monete, 2 - non abbastanza spazio*/
  SELECT P.costituzione
    FROM personaggio AS P
    WHERE id_personaggio = P.id
    INTO costituzione;

    peso_massimo = costituzione/2;
    IF (costituzione % 2 = 1) THEN
      peso_massimo = peso_massimo+1;
    END IF;

  SELECT COUNT(I.id)
    FROM inventario  AS I
    JOIN oggetto AS O ON I.oggetto = O.nome
    WHERE personaggio = id_personaggio
          AND
      (O.tipo_oggetto = 0
        OR O.tipo_oggetto = 1
        OR O.tipo_oggetto = 2
        OR O.tipo_oggetto = 3)
        AND NOT((O.tipo_oggetto = 2 OR O.tipo_oggetto = 3) AND equipaggiato)
    INTO oggetti_posseduti;

    IF (oggetti_posseduti = peso_massimo) THEN
      RETURN 2;
    END IF;

    SELECT I.quantita, I.id
    FROM inventario  AS I
    JOIN oggetto AS O ON I.oggetto = O.nome
    WHERE id_personaggio = I.personaggio
    AND O.tipo_oggetto = 4
    INTO monete, id_mon_c;

    SELECT I.in_vendita
    FROM inventario AS I
    WHERE I.id = id_oggetto
    INTO prezzo_oggetto;

    IF (prezzo_oggetto > monete) THEN
      RETURN 1;
    END IF;

    SELECT I.personaggio
    FROM inventario AS I
    WHERE I.id = id_oggetto
    INTO id_venditore;

    SELECT I.quantita, I.id
    FROM inventario  AS I
    JOIN oggetto AS O ON I.oggetto = O.nome
    WHERE id_venditore = I.personaggio
    AND O.tipo_oggetto = 4
    INTO monete_venditore, id_mon_v;

    SELECT equipaggiato FROM inventario WHERE id_venditore=personaggio AND id_oggetto=id INTO equipped;

    IF equipped THEN
      UPDATE dungeonasdb.inventario SET equipaggiato=FALSE WHERE id=id_oggetto;
    END IF;

    UPDATE dungeonasdb.inventario SET personaggio=id_personaggio, in_vendita=0 WHERE id=id_oggetto;

    UPDATE dungeonasdb.inventario SET quantita=monete-prezzo_oggetto WHERE id = id_mon_c;

    UPDATE dungeonasdb.inventario SET quantita=monete_venditore+prezzo_oggetto WHERE id = id_mon_v;

    return 0;
END;
$$
;

create function vendi(id_oggetto integer, prezzo integer) returns void
	language plpgsql
as $$
BEGIN
 UPDATE dungeonasdb.inventario SET  in_vendita=prezzo WHERE id=id_oggetto;
END;
$$
;

create function ritira(id_oggetto integer) returns void
	language plpgsql
as $$
BEGIN
 UPDATE dungeonasdb.inventario SET  in_vendita=0 WHERE id=id_oggetto;
END;
$$
;

create function search(player integer) returns boolean
	language plpgsql
as $$
DECLARE
      rand INTEGER;
      per INTEGER;
      pf INTEGER;
      item_count INTEGER;
      item_id INTEGER;
      room_count INTEGER;
      room_id INTEGER;
    BEGIN
      SELECT percezione, in_vita FROM dungeonasdb.personaggio WHERE id=player INTO per, pf;
      IF pf < 1 THEN
        RETURN TRUE;
      END IF;
      UPDATE dungeonasdb.personaggio SET in_vita=in_vita-1 WHERE id=player;
      SELECT(CAST(random()*11 AS INTEGER)%20)+1 INTO rand;
      IF rand <= per THEN
        --. query n oggetti nascosti nella stanza
        SELECT count(*) FROM dungeonasdb.posizionato PO
        JOIN dungeonasdb.personaggio PE ON PE.stanza = PO.stanza
        WHERE PO.stanza = PE.stanza AND PE.id = player AND NOT visibilita
        INTO item_count;
        --. query n stanze nascoste collegate alla stanza
        SELECT count(*) FROM dungeonasdb.passaggio
        JOIN dungeonasdb.personaggio P ON da_stanza=stanza
        JOIN dungeonasdb.stanza ON a_stanza=stanza.codice_numerico
        WHERE status = 0 AND tipo_stanza = 2 AND P.id = player
        INTO room_count;

        IF item_count=0 AND room_count=0 THEN RETURN FALSE; END IF;

        IF item_count!=0 AND room_count!=0 THEN
          SELECT(CAST(random()*11 AS INTEGER)%2) INTO rand;
          IF rand = 0 THEN
            item_count = 0;
          ELSE
            room_count = 0;
          END IF;
        END IF;

        IF item_count!=0 THEN
          SELECT PO.id FROM dungeonasdb.posizionato PO
          JOIN dungeonasdb.personaggio PE ON PE.stanza = PO.stanza
          WHERE PO.stanza = PE.stanza AND PE.id = player AND NOT visibilita
          ORDER BY random() LIMIT 1 INTO item_id;

          UPDATE dungeonasdb.posizionato SET visibilita = TRUE WHERE id=item_id;

        ELSEIF room_count!=0 THEN
          SELECT codice_numerico FROM dungeonasdb.passaggio
          JOIN dungeonasdb.personaggio P ON da_stanza=stanza
          JOIN dungeonasdb.stanza ON a_stanza=stanza.codice_numerico
          WHERE status = 0 AND tipo_stanza = 2 AND P.id = player
          ORDER BY random() LIMIT 1 INTO room_id;

          UPDATE dungeonasdb.stanza SET status = 1 WHERE codice_numerico=room_id;
        END IF;
        RETURN TRUE;
      END IF;
      RETURN FALSE;
    END;
$$
;

create function scambia(id_oggetto integer, id_personaggio integer) returns void
	language plpgsql
as $$
DECLARE
  tipo_oggetto VARCHAR(20);
  provvisoria dungeonasdb.inventario%ROWTYPE;
  id_personaggio_2 INTEGER;
  oggetto_scambio VARCHAR(20);
  equipped BOOLEAN;

BEGIN
  SELECT personaggio, scambia, equipaggiato, oggetto
    FROM inventario
    WHERE id = id_oggetto
    INTO id_personaggio_2, oggetto_scambio, equipped, tipo_oggetto;

  IF equipped THEN
    UPDATE dungeonasdb.inventario SET equipaggiato=FALSE WHERE id=id_oggetto;
  END IF;

  DELETE FROM inventario WHERE id = id_oggetto;

  SELECT *
    FROM inventario
    WHERE oggetto = oggetto_scambio
    AND personaggio = id_personaggio
    LIMIT 1
    INTO provvisoria;

  IF provvisoria.equipaggiato THEN
    UPDATE dungeonasdb.inventario SET equipaggiato=FALSE WHERE id = provvisoria.id;
  END IF;

  DELETE FROM inventario WHERE id = provvisoria.id;

  INSERT INTO inventario (personaggio, oggetto) VALUES (id_personaggio_2, oggetto_scambio);
  INSERT INTO inventario (personaggio, oggetto) VALUES (id_personaggio, tipo_oggetto);
END;
$$
;

create function battle(player integer, enemy integer) returns record
	language plpgsql
as $$
DECLARE
    attack INTEGER;
    defence INTEGER;
    damage INTEGER;
    received INTEGER = 0;
    inferted INTEGER;
    A INTEGER;
    t INTEGER;
    enemyName VARCHAR(20);
    logger RECORD;
  BEGIN
    SELECT difesa FROM personaggio WHERE id=player INTO defence;
    FOR attack, damage IN SELECT N.attacco, N.danni FROM collocato C JOIN nemico N ON C.nemico = N.nome JOIN personaggio P ON P.id=player WHERE C.stanza=P.stanza LOOP
      A = attack - defence;
      SELECT(CAST(random()*20 AS INTEGER)%20)+1 INTO t;
      A = A + t;
      IF(A>12)
      THEN UPDATE personaggio
        SET in_vita=in_vita-damage
        WHERE id=player;
      received=damage+received;
      END IF;
    END LOOP;

    SELECT P.attacco, O.punti_attacco FROM personaggio P
      JOIN inventario I ON P.id = I.personaggio
      JOIN oggetto O ON I.oggetto = O.nome
    WHERE equipaggiato AND tipo_oggetto=0 AND P.id = player INTO attack, damage;

    IF(attack ISNULL) THEN
      SELECT P.attacco, O.punti_attacco FROM personaggio P
        JOIN inventario I ON P.id = I.personaggio
        JOIN oggetto O ON I.oggetto = O.nome
      WHERE tipo_oggetto=5 AND P.id = player INTO attack, damage;
    END IF;

    SELECT difesa, nemico FROM collocato C JOIN nemico N ON C.nemico = N.nome WHERE id=enemy INTO defence, enemyName;
    A = attack - defence;
    SELECT(CAST(random()*20 AS INTEGER)%20)+1 INTO t;
    A = A + t;
    IF(A>12) THEN
      UPDATE collocato
      SET in_vita=in_vita-damage
      WHERE id=enemy;

      SELECT in_vita FROM collocato WHERE id=enemy INTO inferted;
      IF(inferted ISNULL) THEN
        inferted=-1;
      ELSE
        inferted=damage;
      END IF;
    END IF;
    SELECT P.nome, received, enemyName, inferted FROM personaggio P WHERE P.id=player INTO logger;
    RETURN logger;
  END;
$$
;

create function poni_in_scambio(id_oggetto integer, scambiante character varying) returns void
	language plpgsql
as $$
BEGIN
 UPDATE dungeonasdb.inventario SET  scambia=scambiante WHERE id=id_oggetto;
END;
$$
;

create function ritira_scambio(id_oggetto integer) returns void
	language plpgsql
as $$
BEGIN
 UPDATE dungeonasdb.inventario SET  scambia=NULL WHERE id=id_oggetto;
END;
$$
;

create function create_room(descr character varying, tipo integer, id bigint) returns integer
	language plpgsql
as $$
DECLARE
    num INTEGER;
BEGIN
        INSERT INTO stanza(descrizione, tipo_stanza, dungeon)
          VALUES(descr, tipo, id)
          RETURNING codice_numerico INTO num;

        RETURN num;
END;
$$
;
