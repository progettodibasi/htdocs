create database dungeonAsDb;
create schema dungeonAsDb;
SET search_path TO dungeonasdb;

CREATE SEQUENCE name_num_seq;
create SEQUENCE dungeon;

create table utente(
  email varchar(50) PRIMARY KEY,
  password varchar(32) NOT NULL,
  nome varchar(20) DEFAULT 'utente'||nextval('name_num_seq'::regclass) UNIQUE
  CHECK (email SIMILAR TO '_%@_%._%')
);

create table stanza(
  codice_numerico serial PRIMARY KEY,
  descrizione varchar(100) NOT NULL,
  tipo_stanza integer NOT NULL,
  dungeon bigint NOT NULL,
  --.0 - mai vista | 1 - vista ma non visitata | 2 - visitata
  status integer DEFAULT 0 NOT NULL
);

create table passaggio(
  da_stanza integer REFERENCES stanza(codice_numerico) ON DELETE CASCADE ON UPDATE CASCADE,
  a_stanza integer REFERENCES stanza(codice_numerico) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY(da_stanza, a_stanza),
  CHECK(da_stanza <> a_stanza)
);

create table personaggio(
  id serial PRIMARY KEY,
  utente varchar(50) REFERENCES utente(email) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
  stanza integer REFERENCES stanza(codice_numerico) ON DELETE NULL ON UPDATE CASCADE,
  punti_esperienza bigint default 0,
  nome varchar(20) NOT NULL,
  descrizione varchar(1000),
  forza integer NOT NULL,
  intelligenza integer NOT NULL,
  costituzione integer NOT NULL,
  agilita integer NOT NULL,
  attacco integer NOT NULL,
  difesa integer NOT NULL,
  percezione integer NOT NULL,
  punti_ferita integer NOT NULL,
  in_vita integer NOT NULL
);

create table nemico(
  nome varchar(20) PRIMARY KEY,
  descrizione varchar(100) NOT NULL,
  attacco integer NOT NULL,
  difesa integer NOT NULL,
  danni integer NOT NULL,
  punti_ferita integer NOT NULL
);

create table collocato(
  id serial PRIMARY KEY,
  stanza integer REFERENCES stanza(codice_numerico) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
  nemico varchar(20) REFERENCES nemico(nome) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
  in_vita integer NOT NULL,
);

create table oggetto(
  nome varchar(20) PRIMARY KEY,
  descrizione varchar(100),
  attacco integer NOT NULL,
  difesa integer NOT NULL,
  percezione integer NOT NULL,
  punti_ferita integer NOT NULL,
  tipo_oggetto integer,
  punti_attacco integer,
  CHECK (attacco BETWEEN -6 AND 6),
  CHECK (difesa BETWEEN -6 AND 6),
  CHECK (percezione BETWEEN -6 AND 6),
  CHECK (punti_ferita BETWEEN -6 AND 6),
  CHECK (punti_attacco > 0)
);

create table posizionato(
  id serial PRIMARY KEY,
  stanza integer REFERENCES stanza(codice_numerico) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
  oggetto varchar(20) REFERENCES oggetto(nome) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
  visibilita boolean NOT NULL,
  quantita INTEGER
);

create table inventario(
  id serial PRIMARY KEY,
  personaggio integer REFERENCES personaggio(id) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
  oggetto varchar(20) REFERENCES oggetto(nome) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
  equipaggiato boolean NOT NULL DEFAULT FALSE,
  in_vendita integer NOT NULL default 0,
  scambia varchar(20) REFERENCES oggetto(nome) ON DELETE CASCADE ON UPDATE CASCADE,
  quantita INTEGER --.x le monete
);

--. TIPI DI OGGETTI
--. 0 attacco | 1 difesa | 2 consumabili | 3 vita | 4 moneta | 5 pugni
--. id | personaggio | oggetto | equipaggiato | in_vendita | scambia

create or REPLACE function dungeonasdb.pg_life_check() returns trigger
	language plpgsql
as $$
DECLARE
  var BIGINT;
  cod INTEGER;
BEGIN
    IF(NEW.in_vita > NEW.punti_ferita) THEN
      UPDATE dungeonasdb.personaggio SET in_vita=punti_ferita WHERE id=NEW.id;
    ELSEIF NEW.in_vita <= 0 THEN
      SELECT dungeon FROM dungeonasdb.stanza S
      JOIN dungeonasdb.personaggio P ON P.stanza = S.codice_numerico
      WHERE id=NEW.id
      INTO var;

      FOR cod IN SELECT S.codice_numerico FROM dungeonasdb.stanza S WHERE S.dungeon=var LOOP
        DELETE FROM dungeonasdb.stanza WHERE codice_numerico = cod;
      END LOOP;
    END IF;
    RETURN NEW;
  END;
$$;

CREATE TRIGGER pg_life_check_trigger
AFTER UPDATE OF in_vita
ON dungeonasdb.personaggio
FOR EACH ROW
EXECUTE PROCEDURE pg_life_check();

create or REPLACE function dungeonasdb.add_exp_enemy() returns trigger
LANGUAGE plpgsql
AS $$
DECLARE
    dmg INTEGER;
  BEGIN
    SELECT danni FROM dungeonasdb.nemico WHERE OLD.nemico = nome INTO dmg;
    UPDATE dungeonasdb.personaggio P SET punti_esperienza=punti_esperienza+dmg WHERE P.stanza = OLD.stanza;
    RETURN NEW;
  END;
$$;

CREATE TRIGGER add_exp_enemy_trigger
AFTER DELETE ON dungeonasdb.collocato
FOR EACH ROW
EXECUTE PROCEDURE dungeonasdb.add_exp_enemy();

create or REPLACE function dungeonasdb.add_exp_room() returns trigger
LANGUAGE plpgsql
AS $$
BEGIN
    IF NEW.tipo_stanza <> 0 AND NEW.status = 2 AND NEW.status <> OLD.status THEN
      UPDATE dungeonasdb.personaggio P SET punti_esperienza=punti_esperienza+10
      WHERE P.stanza = NEW.codice_numerico;
    END IF;
    RETURN NEW;
  END;
$$;

CREATE TRIGGER add_exp_room_trigger
AFTER UPDATE OF status
ON dungeonasdb.stanza
FOR EACH ROW
EXECUTE PROCEDURE dungeonasdb.add_exp_room();

CREATE OR REPLACE FUNCTION dungeonasdb.connect_room(da INTEGER, a INTEGER) RETURNS VOID
LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO passaggio(da_stanza, a_stanza) VALUES(da, a);
END;
$$;

create or REPLACE function dungeonasdb.create_room(descr character varying, tipo integer, id BIGINT) returns integer
LANGUAGE plpgsql
AS $$
DECLARE
    num INTEGER;
BEGIN
        INSERT INTO stanza(descrizione, tipo_stanza, dungeon)
          VALUES(descr, tipo, id)
          RETURNING codice_numerico INTO num;

        RETURN num;
END;
$$;

create function dungeonasdb.create_dungeon() returns integer
LANGUAGE plpgsql
AS $$
DECLARE
  max INTEGER = 4;
  cn_da INTEGER;
  flag INTEGER[] = '{0, 0, 0}';
BEGIN
  --.radice
  SELECT dungeonasdb.create_room('Stanza iniziale', 0, nextval('dungeon'::REGCLASS)) INTO cn_da;
  --.SETTA IL SEED
  PERFORM setseed(extract(SECONDS FROM current_timestamp)/100);

  SELECT create_dungeon_child(max, cn_da, flag) INTO flag;
  IF flag[2]=0
    THEN UPDATE stanza SET descrizione='stanza finale', tipo_stanza=3 WHERE flag[3]=codice_numerico;
    END IF;

  RETURN cn_da;
END;
$$;

create function dungeonasdb.create_dungeon_child(max integer, cn_da integer, fl integer[]) returns integer[]
LANGUAGE plpgsql
AS $$
DECLARE
  num_rm INTEGER;
  type_rm INTEGER;
  cn_a INTEGER;
  flag INTEGER[] = fl;
BEGIN
  IF max<2
    THEN RETURN flag;
  --.Quante stanze collego (da 2 a 4)?
  ELSEIF max=4
    THEN SELECT(CAST(random()*11 AS INTEGER)%3)+2 INTO num_rm;
  --.(da 1 a 2)?
  ELSEIF max=3
    THEN SELECT(CAST(random()*11 AS INTEGER)%2)+1 INTO num_rm;
  --.(da 0 a 1)?
  ELSE
    SELECT(CAST(random()*11 AS INTEGER)%2) INTO num_rm;
    END IF;

  IF num_rm=0
    THEN RETURN flag;
    END IF;

  FOR i in 1..num_rm LOOP
    --.random tipo stanza (1 - normale, 2 - nascosta, 3 - finale)
    IF (flag[1]<>4)AND(flag[2]<>1)AND(max<=3)
      THEN SELECT(CAST(random()*11 AS INTEGER)%3)+1 INTO type_rm;
    ELSEIF (flag[1]<>4)AND(flag[2]=1)
      THEN SELECT(CAST(random()*11 AS INTEGER)%2)+1 INTO type_rm;
    ELSEIF (flag[1]=4)AND(flag[2]<>1)
      THEN SELECT(CAST(random()*11 AS INTEGER)%2)*2+1 INTO type_rm;
    ELSE
      type_rm=1;
      END IF;
    IF type_rm=3
      THEN SELECT dungeonasdb.create_room('Stanza finale', type_rm, currval('dungeon'::REGCLASS)) INTO cn_a;
      ELSE SELECT dungeonasdb.create_room('Stanza '||max||'-'||i, type_rm, currval('dungeon'::REGCLASS)) INTO cn_a;
    END IF;
    PERFORM connect_room(cn_da, cn_a);
    IF type_rm=2
      THEN flag[1]=flag[1]+1;
      END IF;
    IF type_rm=3
      THEN flag[2]=flag[2]+1;
      END IF;
    flag[3] = cn_a;
    IF type_rm<>3
      THEN SELECT create_dungeon_child(max-1, cn_a, flag) INTO flag;
      END IF;
  END LOOP;
  RETURN flag;
END;
$$;

CREATE OR REPLACE FUNCTION dungeonasdb.room_status_upd() RETURNS TRIGGER
  LANGUAGE plpgsql
  AS $$
  BEGIN
    UPDATE dungeonasdb.stanza
      SET status = 2
    WHERE NEW.stanza = stanza.codice_numerico;

    UPDATE dungeonasdb.stanza
      SET status = 1
      FROM dungeonasdb.passaggio P
    WHERE stanza.codice_numerico = P.a_stanza
      AND NEW.stanza = P.da_stanza
      AND stanza.status = 0
      AND stanza.tipo_stanza <> 2;

    RETURN NEW;
  END;
  $$;

CREATE TRIGGER room_status_trigger
  AFTER INSERT OR UPDATE OF stanza
  ON personaggio
  FOR EACH ROW
  EXECUTE PROCEDURE room_status_upd();

  create or replace function dungeonasdb.create_pg(forza1 integer, intelligenza1 integer, costituzione1 integer, agilita1 integer, utentefk character varying, nome1 character varying, descrizione1 character varying) returns void
  LANGUAGE plpgsql
  AS $$
  DECLARE
    attacco1 INTEGER;
    difesa1 INTEGER;
    percezione1 INTEGER;
    punti_ferita1 INTEGER;
    id_pg INTEGER;

  BEGIN
          IF((forza1 + agilita1)%2<>0)
            THEN attacco1 = ((forza1 + agilita1)/2)+1;
          ELSE attacco1 = (forza1 + agilita1)/2;
          END IF;

          IF((costituzione1 + agilita1)%2<>0)
            THEN difesa1 = ((costituzione1 + agilita1)/2)+1;
          ELSE difesa1 = (costituzione1 + agilita1)/2;
          END IF;

          percezione1 = intelligenza1;
          punti_ferita1 = costituzione1;
          INSERT INTO personaggio(utente, stanza, nome, descrizione, forza, intelligenza, costituzione, agilita, attacco, difesa, percezione, punti_ferita, in_vita)
          VALUES(utentefk, create_dungeon(), nome1, descrizione1, forza1, intelligenza1, costituzione1, agilita1, attacco1, difesa1, percezione1, punti_ferita1, punti_ferita1)
          RETURNING id INTO id_pg;

          INSERT INTO inventario(personaggio, oggetto)
          VALUES(id_pg ,'spada arrugginita');
          INSERT INTO inventario(personaggio, oggetto)
          VALUES(id_pg ,'razione');
          INSERT INTO inventario(personaggio, oggetto, quantita)
          VALUES(id_pg ,'moneta', 0);
          INSERT INTO inventario(personaggio, oggetto)
          VALUES(id_pg ,'pugni');
  END;
  $$;

  create or REPLACE function dungeonasdb.battle(player integer, enemy integer)
    returns RECORD
  LANGUAGE plpgsql
  AS $$
  DECLARE
      attack INTEGER;
      defence INTEGER;
      damage INTEGER;
      received INTEGER = 0;
      inferted INTEGER;
      A INTEGER;
      t INTEGER;
      enemyName VARCHAR(20);
      logger RECORD;
    BEGIN
      SELECT difesa FROM personaggio WHERE id=player INTO defence;
      FOR attack, damage IN SELECT N.attacco, N.danni FROM collocato C JOIN nemico N ON C.nemico = N.nome JOIN personaggio P ON P.id=player WHERE C.stanza=P.stanza LOOP
        A = attack - defence;
        SELECT(CAST(random()*20 AS INTEGER)%20)+1 INTO t;
        A = A + t;
        IF(A>12)
        THEN UPDATE personaggio
          SET in_vita=in_vita-damage
          WHERE id=player;
        received=damage+received;
        END IF;
      END LOOP;

      SELECT P.attacco, O.punti_attacco FROM personaggio P
        JOIN inventario I ON P.id = I.personaggio
        JOIN oggetto O ON I.oggetto = O.nome
      WHERE equipaggiato AND tipo_oggetto=0 AND P.id = player INTO attack, damage;

      IF(attack ISNULL) THEN
        SELECT P.attacco, O.punti_attacco FROM personaggio P
          JOIN inventario I ON P.id = I.personaggio
          JOIN oggetto O ON I.oggetto = O.nome
        WHERE tipo_oggetto=5 AND P.id = player INTO attack, damage;
      END IF;

      SELECT difesa, nemico FROM collocato C JOIN nemico N ON C.nemico = N.nome WHERE id=enemy INTO defence, enemyName;
      A = attack - defence;
      SELECT(CAST(random()*20 AS INTEGER)%20)+1 INTO t;
      A = A + t;
      IF(A>12) THEN
        UPDATE collocato
        SET in_vita=in_vita-damage
        WHERE id=enemy;

        SELECT in_vita FROM collocato WHERE id=enemy INTO inferted;
        IF(inferted ISNULL) THEN
          inferted=-1;
        ELSE
          inferted=damage;
        END IF;
      END IF;
      SELECT P.nome, received, enemyName, inferted FROM personaggio P WHERE P.id=player INTO logger;
      RETURN logger;
    END;
  $$;

  CREATE OR REPLACE FUNCTION remove_enemy() RETURNS TRIGGER
    LANGUAGE plpgsql
    AS $$
    BEGIN
      IF NEW.in_vita<=0 THEN
        DELETE FROM dungeonasdb.collocato WHERE NEW.id = collocato.id;
        RETURN NULL;
      END IF;
      RETURN NEW;
    END;
    $$;

  CREATE TRIGGER remove_enemy_trigger
    AFTER UPDATE OF in_vita
    ON dungeonasdb.collocato
    FOR EACH ROW
    EXECUTE PROCEDURE remove_enemy();

    create or REPLACE function dungeonasdb.inv_dim_check() returns trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE
        cur_dim INTEGER;
        max_dim INTEGER;
        tipo INTEGER;
      BEGIN
        SELECT tipo_oggetto FROM oggetto
        JOIN inventario ON oggetto.nome = NEW.oggetto
        INTO tipo;
        IF tipo=4 OR tipo=5 THEN RETURN NEW; END IF;

        SELECT count(*) FROM dungeonasdb.inventario I
        JOIN dungeonasdb.oggetto O ON I.oggetto = O.nome
        WHERE O.tipo_oggetto < 4 AND NEW.personaggio = I.personaggio AND NOT((O.tipo_oggetto = 2 OR O.tipo_oggetto = 3) AND equipaggiato)
        INTO cur_dim;

        cur_dim = cur_dim + 1;
        SELECT costituzione FROM dungeonasdb.personaggio WHERE NEW.personaggio = id INTO max_dim;

        IF (max_dim%2)<>0 THEN max_dim = max_dim/2 + 1;
        ELSE max_dim = max_dim/2;
        END IF;

        IF(cur_dim > max_dim) THEN
          RAISE EXCEPTION 'Non puoi portare altri oggetti!';
        END IF;

        RETURN NEW;
      END;
    $$;

    CREATE TRIGGER inv_dim_check_trigger
    BEFORE INSERT
    ON dungeonasdb.inventario
    FOR EACH ROW
    EXECUTE PROCEDURE inv_dim_check();

    create or replace FUNCTION inv_equip() RETURNS TRIGGER
    LANGUAGE plpgsql
    AS $$
      DECLARE
        tipo INTEGER;
        swap_item RECORD;
      BEGIN
        IF NEW.equipaggiato THEN
          SELECT tipo_oggetto FROM dungeonasdb.oggetto O WHERE O.nome = NEW.oggetto INTO tipo;
          IF tipo < 2 THEN
            SELECT id, personaggio, oggetto FROM dungeonasdb.inventario I
            JOIN dungeonasdb.oggetto O ON O.nome = I.oggetto
            WHERE I.equipaggiato AND I.id <> NEW.id AND O.tipo_oggetto = tipo
            INTO swap_item;
            UPDATE dungeonasdb.inventario SET equipaggiato=FALSE WHERE id = swap_item.id;
          END IF;
        END IF;

        RETURN NEW;
      END;
    $$;

    CREATE TRIGGER inv_equip_trigger
    AFTER UPDATE OF equipaggiato
    ON dungeonasdb.inventario
    FOR EACH ROW
    EXECUTE PROCEDURE inv_equip();

    create or REPLACE function dungeonasdb.update_stats_pg() returns trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE
        tipo INTEGER;
        bonus RECORD;
      BEGIN
        SELECT tipo_oggetto FROM dungeonasdb.oggetto WHERE nome=NEW.oggetto INTO tipo;
        SELECT attacco, difesa, percezione, punti_ferita FROM dungeonasdb.oggetto
        WHERE nome=NEW.oggetto INTO bonus;
        IF(NEW.equipaggiato) THEN
          IF(tipo<3) THEN
            UPDATE dungeonasdb.personaggio SET attacco      = attacco      + bonus.attacco,
                                   difesa       = difesa       + bonus.difesa ,
                                   percezione   = percezione   + bonus.percezione,
                                   punti_ferita = punti_ferita + bonus.punti_ferita
            WHERE id=NEW.personaggio;
          ELSE
            UPDATE dungeonasdb.personaggio SET in_vita = in_vita + bonus.punti_ferita
            WHERE id=NEW.personaggio;
          END IF;
        ELSE
          IF(tipo<3) THEN
            UPDATE dungeonasdb.personaggio SET attacco      = attacco      - bonus.attacco,
                                   difesa       = difesa       - bonus.difesa,
                                   percezione   = percezione   - bonus.percezione,
                                   punti_ferita = punti_ferita - bonus.punti_ferita
            WHERE id=NEW.personaggio;
          END IF;
        END IF;

        RETURN NEW;
      END;
    $$;

    CREATE TRIGGER update_stats_pg_trigger
    AFTER UPDATE OF equipaggiato
    ON dungeonasdb.inventario
    FOR EACH ROW
    EXECUTE PROCEDURE update_stats_pg();

    create or REPLACE function dungeonasdb.delete_item(item integer) returns void
    LANGUAGE plpgsql
    AS $$
    DECLARE
        eq BOOLEAN;
      BEGIN
        SELECT equipaggiato FROM dungeonasdb.inventario WHERE id=item INTO eq;
        IF eq THEN
          UPDATE dungeonasdb.inventario SET equipaggiato=FALSE WHERE id=item;
        END IF;
        DELETE FROM dungeonasdb.inventario WHERE item = id;
      END;
    $$;

    CREATE OR REPLACE FUNCTION delete_cons_bonus() RETURNS TRIGGER
    LANGUAGE plpgsql
    AS $$
      DECLARE
        item INTEGER;
      BEGIN
        FOR item IN SELECT id FROM dungeonasdb.inventario I JOIN dungeonasdb.oggetto ON nome = I.oggetto WHERE equipaggiato AND (tipo_oggetto = 2 OR tipo_oggetto = 3) AND personaggio = NEW.id LOOP
          PERFORM dungeonasdb.delete_item(item);
        END LOOP;

        RETURN NEW;
      END;
    $$;

    CREATE TRIGGER delete_cons_bonus_trigger
    AFTER UPDATE OF stanza
    ON dungeonasdb.personaggio
    FOR EACH ROW
    EXECUTE PROCEDURE delete_cons_bonus();

    CREATE OR REPLACE FUNCTION update_coin() RETURNS TRIGGER
    LANGUAGE plpgsql
    AS $$
      DECLARE
        tipo INTEGER;
        id1 INTEGER;
        money dungeonasdb.inventario%ROWTYPE;
      BEGIN
        SELECT tipo_oggetto FROM dungeonasdb.oggetto WHERE nome = NEW.oggetto INTO tipo;

        IF(tipo = 4) THEN
          SELECT id FROM dungeonasdb.inventario WHERE oggetto = 'moneta' AND NEW.personaggio = personaggio INTO id1;
          IF id1 NOTNULL THEN
            UPDATE dungeonasdb.inventario I SET quantita = quantita+NEW.quantita WHERE I.id=id1 RETURNING * INTO money;
            DELETE FROM dungeonasdb.inventario WHERE id=id1;
            RETURN money;
          END IF;
        END IF;

        RETURN NEW;
      END;
    $$;

    CREATE TRIGGER update_coin_trigger
    BEFORE INSERT
    ON dungeonasdb.inventario
    FOR EACH ROW
    EXECUTE PROCEDURE update_coin();

    create or REPLACE function fill_room() returns trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE
          hide_coin INTEGER;
          item dungeonasdb.oggetto%ROWTYPE;
          max INTEGER;
          enemy dungeonasdb.nemico%ROWTYPE;
          dmg_limit INTEGER;
      BEGIN
        IF NEW.status <> 2 THEN  RETURN NEW; END IF;
        IF (NEW.tipo_stanza=0) OR OLD.status=2 THEN RETURN NEW; END IF;
        IF(NEW.tipo_stanza=3) THEN
          SELECT * FROM dungeonasdb.nemico WHERE punti_ferita>15 ORDER BY random() LIMIT 1 INTO enemy;
          INSERT INTO dungeonasdb.collocato(stanza, nemico, in_vita)
            VALUES(NEW.codice_numerico, enemy.nome, enemy.punti_ferita);
        RETURN NEW;
        END IF;
        --.RIEMPI OGGETTI
        --.da 0 a 5 oggetti
        SELECT(CAST(random()*100 AS INTEGER)%100) INTO max; --.da 0 a 99
        IF max>=0  AND max<=14 THEN max = 0;
        ELSEIF max>=15 AND max<=34 THEN max = 1;
        ELSEIF max>=35 AND max<=69 THEN max = 2;
        ELSEIF max>=70 AND max<=84 THEN max = 3;
        ELSEIF max>=85 AND max<=94 THEN max = 4;
        ELSEIF max>=95 AND max<=99 THEN max = 5;
        END IF;
        FOR i in 0..max LOOP
          SELECT(CAST(random()*11 AS INTEGER)%2) INTO hide_coin;
          SELECT * FROM dungeonasdb.oggetto WHERE tipo_oggetto < 4 ORDER BY random() LIMIT 1 INTO item;
          IF hide_coin=1 THEN
            INSERT INTO dungeonasdb.posizionato(stanza, oggetto, visibilita)
            VALUES(NEW.codice_numerico, item.nome, TRUE);
          ELSE
            INSERT INTO dungeonasdb.posizionato(stanza, oggetto, visibilita)
            VALUES(NEW.codice_numerico, item.nome, FALSE);
          END IF;
        END LOOP;
        --.da 0 a 10 monete
        SELECT(CAST(random()*11 AS INTEGER)%2) INTO hide_coin;
        IF hide_coin=1 THEN
          SELECT(CAST(random()*11 AS INTEGER)%10)+1 INTO max;
          INSERT INTO dungeonasdb.posizionato(stanza, oggetto, visibilita, quantita)
            VALUES(NEW.codice_numerico, 'moneta', TRUE, max);
        END IF;
        --.RIEMPI NEMICI
        --.stanza finale da fare
        --.random da 1 a 3 nemici
        SELECT(CAST(random()*11 AS INTEGER)%3)+1 INTO max;
        SELECT punti_ferita FROM dungeonasdb.personaggio WHERE stanza = NEW.codice_numerico INTO dmg_limit;
        FOR i in 1..max LOOP
          SELECT * FROM dungeonasdb.nemico WHERE danni<(dmg_limit/2) ORDER BY random() LIMIT 1 INTO enemy;
          IF enemy ISNULL THEN RETURN NEW; END IF;
          INSERT INTO dungeonasdb.collocato(stanza, nemico, in_vita)
                VALUES(NEW.codice_numerico, enemy.nome, enemy.punti_ferita);
          dmg_limit = dmg_limit - enemy.danni;
        END LOOP;
        RETURN new;

      END;
    $$;

    CREATE TRIGGER fill_room_trigger
      BEFORE UPDATE OF status
      ON stanza
      FOR EACH ROW
      EXECUTE PROCEDURE fill_room();

      create or replace function dungeonasdb.search(player integer) returns BOOLEAN
        LANGUAGE plpgsql
        AS $$
        DECLARE
            rand INTEGER;
            per INTEGER;
            pf INTEGER;
            item_count INTEGER;
            item_id INTEGER;
            room_count INTEGER;
            room_id INTEGER;
          BEGIN
            SELECT percezione, in_vita FROM dungeonasdb.personaggio WHERE id=player INTO per, pf;
            IF pf < 1 THEN
              RETURN TRUE;
            END IF;
            UPDATE dungeonasdb.personaggio SET in_vita=in_vita-1 WHERE id=player;
            SELECT(CAST(random()*11 AS INTEGER)%20)+1 INTO rand;
            IF rand <= per THEN
              --. query n oggetti nascosti nella stanza
              SELECT count(*) FROM dungeonasdb.posizionato PO
              JOIN dungeonasdb.personaggio PE ON PE.stanza = PO.stanza
              WHERE PO.stanza = PE.stanza AND PE.id = player AND NOT visibilita
              INTO item_count;
              --. query n stanze nascoste collegate alla stanza
              SELECT count(*) FROM dungeonasdb.passaggio
              JOIN dungeonasdb.personaggio P ON da_stanza=stanza
              JOIN dungeonasdb.stanza ON a_stanza=stanza.codice_numerico
              WHERE status = 0 AND tipo_stanza = 2 AND P.id = player
              INTO room_count;

              IF item_count=0 AND room_count=0 THEN RETURN FALSE; END IF;

              IF item_count!=0 AND room_count!=0 THEN
                SELECT(CAST(random()*11 AS INTEGER)%2) INTO rand;
                IF rand = 0 THEN
                  item_count = 0;
                ELSE
                  room_count = 0;
                END IF;
              END IF;

              IF item_count!=0 THEN
                SELECT PO.id FROM dungeonasdb.posizionato PO
                JOIN dungeonasdb.personaggio PE ON PE.stanza = PO.stanza
                WHERE PO.stanza = PE.stanza AND PE.id = player AND NOT visibilita
                ORDER BY random() LIMIT 1 INTO item_id;

                UPDATE dungeonasdb.posizionato SET visibilita = TRUE WHERE id=item_id;

              ELSEIF room_count!=0 THEN
                SELECT codice_numerico FROM dungeonasdb.passaggio
                JOIN dungeonasdb.personaggio P ON da_stanza=stanza
                JOIN dungeonasdb.stanza ON a_stanza=stanza.codice_numerico
                WHERE status = 0 AND tipo_stanza = 2 AND P.id = player
                ORDER BY random() LIMIT 1 INTO room_id;

                UPDATE dungeonasdb.stanza SET status = 1 WHERE codice_numerico=room_id;
              END IF;
              RETURN TRUE;
            END IF;
            RETURN FALSE;
          END;
        $$;

        CREATE OR REPLACE FUNCTION dungeonasdb.compra(id_oggetto INTEGER, id_personaggio INTEGER) returns INTEGER
        LANGUAGE plpgsql
        AS $$
        DECLARE
          costituzione INTEGER;
          monete INTEGER;
          prezzo_oggetto INTEGER;
          esito INTEGER;
          oggetti_posseduti INTEGER;
          peso_massimo INTEGER;
          id_venditore INTEGER;
          monete_venditore INTEGER;
          id_mon_c INTEGER;
          id_mon_v INTEGER;

        BEGIN
          esito = 0; /*0 - oggetto acquistato, 1 - non abbastanza monete, 2 - non abbastanza spazio*/
          SELECT P.costituzione
            FROM personaggio AS P
            WHERE id_personaggio = P.id
            INTO costituzione;

            peso_massimo = costituzione/2;
            IF (costituzione % 2 = 1) THEN
              peso_massimo = peso_massimo+1;
            END IF;

          SELECT COUNT(I.id)
            FROM inventario  AS I
            JOIN oggetto AS O ON I.oggetto = O.nome
            WHERE personaggio = id_personaggio
                  AND
              (O.tipo_oggetto = 0
                OR O.tipo_oggetto = 1
                OR O.tipo_oggetto = 2
                OR O.tipo_oggetto = 3)
                AND NOT((O.tipo_oggetto = 2 OR O.tipo_oggetto = 3) AND equipaggiato)
            INTO oggetti_posseduti;

            IF (oggetti_posseduti = peso_massimo) THEN
              RETURN 2;
            END IF;

            SELECT I.quantita, I.id
            FROM inventario  AS I
            JOIN oggetto AS O ON I.oggetto = O.nome
            WHERE id_personaggio = I.personaggio
            AND O.tipo_oggetto = 4
            INTO monete, id_mon_c;

            SELECT I.in_vendita
            FROM inventario AS I
            WHERE I.id = id_oggetto
            INTO prezzo_oggetto;

            IF (prezzo_oggetto > monete) THEN
              RETURN 1;
            END IF;

            SELECT I.personaggio
            FROM inventario AS I
            WHERE I.id = id_oggetto
            INTO id_venditore;

            SELECT I.quantita, I.id
            FROM inventario  AS I
            JOIN oggetto AS O ON I.oggetto = O.nome
            WHERE id_venditore = I.personaggio
            AND O.tipo_oggetto = 4
            INTO monete_venditore, id_mon_v;

            SELECT equipaggiato FROM inventario WHERE id_venditore=personaggio AND id_oggetto=id INTO equipped;

            IF equipped THEN
              UPDATE dungeonasdb.inventario SET equipaggiato=FALSE WHERE id=id_oggetto;
            END IF;

            UPDATE dungeonasdb.inventario SET personaggio=id_personaggio, in_vendita=0 WHERE id=id_oggetto;

            UPDATE dungeonasdb.inventario SET quantita=monete-prezzo_oggetto WHERE id = id_mon_c;

            UPDATE dungeonasdb.inventario SET quantita=monete_venditore+prezzo_oggetto WHERE id = id_mon_v;

            return 0;
        END;
        $$;


        CREATE OR REPLACE FUNCTION dungeonasdb.vendi(id_oggetto INTEGER, prezzo INTEGER) returns VOID
        LANGUAGE plpgsql
        AS $$
        BEGIN
         UPDATE dungeonasdb.inventario SET  in_vendita=prezzo WHERE id=id_oggetto;
        END;
        $$;

        CREATE OR REPLACE FUNCTION dungeonasdb.ritira(id_oggetto INTEGER) returns VOID
        LANGUAGE plpgsql
        AS $$
        BEGIN
         UPDATE dungeonasdb.inventario SET  in_vendita=0 WHERE id=id_oggetto;
        END;
        $$;

        CREATE OR REPLACE FUNCTION dungeonasdb.scambia(id_oggetto INTEGER, id_personaggio INTEGER) returns VOID
        LANGUAGE plpgsql
        AS $$
        DECLARE
          tipo_oggetto VARCHAR(20);
          provvisoria dungeonasdb.inventario%ROWTYPE;
          id_personaggio_2 INTEGER;
          oggetto_scambio VARCHAR(20);
          equipped BOOLEAN;

        BEGIN
          SELECT personaggio, scambia, equipaggiato, oggetto
            FROM inventario
            WHERE id = id_oggetto
            INTO id_personaggio_2, oggetto_scambio, equipped, tipo_oggetto;

          IF equipped THEN
            UPDATE dungeonasdb.inventario SET equipaggiato=FALSE WHERE id=id_oggetto;
          END IF;

          DELETE FROM inventario WHERE id = id_oggetto;

          SELECT *
            FROM inventario
            WHERE oggetto = oggetto_scambio
            AND personaggio = id_personaggio
            LIMIT 1
            INTO provvisoria;

          IF provvisoria.equipaggiato THEN
            UPDATE dungeonasdb.inventario SET equipaggiato=FALSE WHERE id = provvisoria.id;
          END IF;

          DELETE FROM inventario WHERE id = provvisoria.id;

          INSERT INTO inventario (personaggio, oggetto) VALUES (id_personaggio_2, oggetto_scambio);
          INSERT INTO inventario (personaggio, oggetto) VALUES (id_personaggio, tipo_oggetto);
        END;
        $$;

        CREATE OR REPLACE FUNCTION dungeonasdb.poni_in_scambio(id_oggetto INTEGER, scambiante VARCHAR(20)) returns VOID
        LANGUAGE plpgsql
        AS $$
        BEGIN
         UPDATE dungeonasdb.inventario SET  scambia=scambiante WHERE id=id_oggetto;
        END;
        $$;
