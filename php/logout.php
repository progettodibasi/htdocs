<!DOCTYPE html>
<?php
  session_start();
 ?>
<html>
<head>
  <title>logout</title>
  <link rel="icon" href="/../css/image/icona.ico" />
  <link href="/../css/style.css" rel="stylesheet" type="text/css" />
</head>
  <body>
    <div class="latoA">
      <div class="corpo">
          <img src="/../css/image/loghi/Tit.png" alt="Dungeon As Database">

        <pre>
          <?php
            $database = pg_connect("host=localhost port=5432 dbname=dungeonasdb user=postgres password=postgres")
               or die("Impossibile connettersi.");
            $path_query = pg_query($database, "SET search_path TO dungeonasdb");
           ?>
        </pre>
        <div>
          <?php
            session_unset();
            echo "<script type ='text/javascript'>
              sessionStorage.clear();
              window.location.href='/../index.html';
            </script>";
           ?>
        </div>
      </div>
    </div>
  </body>
</html>
