<!DOCTYPE html>
<?php
  session_start();
 ?>
<html>
<head>
  <title>Create Pg</title>
  <link rel="icon" href="/../css/image/icona.ico" />
  <link href="/../css/style.css" rel="stylesheet" type="text/css" />
</head>
  <script type="text/javascript" src="/javascript/user_query.js"></script>
  <body>
    <div class="corpo">
        <img src="/../css/image/loghi/Tit.png" alt="Dungeon As Database">

      <pre>
        <?php
          $param = array($_SESSION["email"]);
          $database = pg_connect("host=localhost port=5432 dbname=dungeonasdb user=postgres password=postgres")
             or die("Impossibile connettersi.");
          $path_query = pg_query($database, "SET search_path TO dungeonasdb");
         ?>
      </pre>
      <div>
        <?php
          $pg_query = pg_query_params($database, "SELECT * FROM dungeonasdb.personaggio WHERE utente = $1 ORDER BY nome;", $param);
          $num_row = pg_num_rows($pg_query);
          echo "<script type ='text/javascript'>
                var pgList = '';
                sessionStorage.clear();
                sessionStorage.setItem('playerList', pgList);
                sessionStorage.setItem('numPg', $num_row);
            </script>";
          while ($pg_row = pg_fetch_row($pg_query)) {
            $js_array = json_encode($pg_row);
            echo "<script type ='text/javascript'>
              addToList($js_array);
            </script>";
          }
          /*SOLUZIONE ALTERNATIVA
          $num_row = 0;
          while ($pg_row = pg_fetch_row($pg_query)) {
            $js_array = json_encode($pg_row);
            echo "<script type ='text/javascript'>
              addToList($num_row, $js_array);
            </script>";
            $num_row = $num_row + 1;
          }*/
          echo "<script type ='text/javascript'>
            window.location.href='/../user_homepage.html';
          </script>";
         ?>
      </div>
    </div>
  </body>
</html>
