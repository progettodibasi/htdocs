<!DOCTYPE html>
<?php
  session_start();
 ?>
<html>
  <head>
    <title>sel_pg</title>
    <link rel="icon" href="/../css/image/icona.ico" />
    <link href="/../css/style.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <pre>
      <?php
        $param = array($_SESSION["personaggio"]);
        $database = pg_connect("host=localhost port=5432 dbname=dungeonasdb user=postgres password=postgres")
           or die("Impossibile connettersi.");
        $path_query = pg_query($database, "SET search_path TO dungeonasdb");
      ?>
    </pre>
    <div class="corpo">
      <?php
      $delete_dungeon = pg_query_params($database, "UPDATE personaggio SET in_vita=0 WHERE id=$1;" , $param);
      $new_game_query = pg_query_params($database, "UPDATE personaggio SET stanza=create_dungeon(), in_vita=punti_ferita WHERE id=$1;", $param);
      echo "<script type ='text/javascript'>
        window.location.href='/../game.html';
      </script>";
      ?>
    </div>
  </body>
</html>
